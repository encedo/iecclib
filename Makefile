include fields/Makerules
include curves/Makerules

FIELD_FILES=$(patsubst %,fields/%.X,$(FIELD_FILES_RAW))
CURVE_FILES=$(patsubst %,curves/%.X,$(CURVE_FILES_RAW))
COMMON_FILES=bigint.X curve.X field.X ecclib.X utils.X

OBJECTS=$(patsubst %.X,%.s.o,$(COMMON_FILES)) $(patsubst %.X,%.s.o,$(FIELD_FILES)) \
	$(patsubst %.X,%.s.o,$(CURVE_FILES))

OBJECTS_D=$(patsubst %.X,%.d.o,$(COMMON_FILES)) $(patsubst %.X,%.d.o,$(FIELD_FILES)) \
	$(patsubst %.X,%.d.o,$(CURVE_FILES))

OBJECTS_P=$(patsubst %.X,%.o,$(COMMON_FILES)) $(patsubst %.X,%.o,$(FIELD_FILES)) \
	$(patsubst %.X,%.o,$(CURVE_FILES))

ifdef USE_64BIT_MATH
USE_64BIT_FLAGS=-DUSE_64BIT_MATH
else
USE_64BIT_FLAGS=
endif
CFLAGS=
REAL_CFLAGS=$(USE_64BIT_FLAGS) $(CFLAGS)
LDFLAGS=
LDEXTRA=
PREFIX=/usr/local
MAJORVERSION=1
MINORVERSION=0
PATCH=0
STATICLIB=libiecclib.a
STATICLIB_D=libiecclib_s.a
DYNAMICLIB_BASE=libiecclib.so
DYNAMICLIB=$(DYNAMICLIB_BASE).$(MAJORVERSION).$(MINORVERSION).$(PATCH)
DYNAMICLIB_SONAME=$(DYNAMICLIB_BASE).$(MAJORVERSION)
HEADERFILE=iecclib.h
PCFILE=iecclib.pc

all: $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D) ecclib.test ecclib.test-opt ecclib.speed ecclib.test-ct \
	ecclib.test-ctd ecclib.test-cts

install: $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D) $(HEADERFILE) $(PCFILE)
	install -D -t $(PREFIX)/lib $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D)
	install -D -t $(PREFIX)/lib/pkgconfig $(PCFILE)
	install -D -t $(PREFIX)/include $(HEADERFILE)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_SONAME).$(MINORVERSION)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_SONAME)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_BASE)

$(PCFILE): $(PCFILE).in
	sed -r -e "s!%%PREFIX%%!$(PREFIX)!" <$^ >$@

$(DYNAMICLIB): ecclib-2.d.o
	gcc -fPIC -shared -Wl,-soname,$(DYNAMICLIB_SONAME) -o $@ $^  $(LDEXTRA)

$(STATICLIB): ecclib-2.s.o
	ar cvrs $@ $^

$(STATICLIB_D): ecclib-2.d.o
	ar cvrs $@ $^

ecclib.test: $(OBJECTS_P) test.o 
	g++ -fprofile-arcs -ftest-coverage -o $@ $^ $(LDFLAGS)

ecclib.test-ct:  ct-test.o $(OBJECTS_P)
	g++ -fprofile-arcs -ftest-coverage -o $@ $^ $(LDFLAGS)

ecclib.test-ctd: ct-test.d.o ecclib.so
	g++ -o $@ $^ $(LDFLAGS)

ecclib.test-cts: ct-test.s.o $(STATICLIB)
	g++ -o $@ $^ $(LDFLAGS)

ecclib.speed: speedtest.o $(STATICLIB)
	g++ -o $@ $^ $(LDFLAGS)

ecclib.test-opt: $(OBJECTS) test-o.o
	g++ -o $@ $^ $(LDFLAGS)

ecclib-2.d.o: ecclib-1.d.o
	objcopy -w --localize-symbol=!ecclib_* --localize-symbol=* $^ $@

ecclib-2.s.o: ecclib-1.s.o
	objcopy -w --localize-symbol=!ecclib_* --localize-symbol=* $^ $@

ecclib-1.s.o: $(OBJECTS)
	ld -r -o $@ $^

ecclib-1.d.o: $(OBJECTS_D)
	ld -r -o $@ $^

%.d.o: %.c
	gcc -g -Werror -Wall -O3 -std=c99 -fPIC -I. $(REAL_CFLAGS) -c -o $@ $<

%.s.o: %.c
	gcc -g -Werror -Wall -O3 -fno-tree-loop-distribute-patterns -std=c99 -I. $(REAL_CFLAGS) -c -o $@ $<

%.o: %.c
	gcc -g -Werror -Wall -fprofile-arcs -ftest-coverage -std=c99 -I. $(REAL_CFLAGS) -c -o $@ $<

test.o: test.cpp
	g++ -g -Werror -Wall -fprofile-arcs -ftest-coverage -std=c++11 -DTEST_ECCLIB_CODE -I. -c -o $@ $<

ct-test.o: ct-test.cpp
	g++ -g -Werror -Wall -fprofile-arcs -ftest-coverage -std=c++11 -DTEST_ECCLIB_CODE -I. -c -o $@ $<

ct-test.d.o: ct-test.cpp
	g++ -g -Werror -Wall -fPIC -std=c++11 -DTEST_ECCLIB_CODE -I. -c -o $@ $<

ct-test.s.o: ct-test.cpp
	g++ -g -Werror -Wall -std=c++11 -DTEST_ECCLIB_CODE -I. -c -o $@ $<

test-o.o: test.cpp
	g++ -g -Werror -Wall -O3 -std=c++11 -I. -DTEST_ECCLIB_CODE -c -o $@ $<

speedtest.o: speedtest.cpp
	g++ -g -Werror -Wall -O3 -std=c++11 -I.  -c -o $@ $<

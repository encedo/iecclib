static inline void _point_load_cond(point_t* out, const point_t* in, uint32_t flag)
{
	FIELD_LOAD_COND(&out->x, &in->x, flag);
	FIELD_LOAD_COND(&out->y, &in->y, flag);
	FIELD_LOAD_COND(&out->z, &in->z, flag);
	FIELD_LOAD_COND(&out->t, &in->t, flag);
}

static inline void _point_zero(point_t* point)
{
	FIELD_ZERO(&point->x);
	FIELD_LOAD_SMALL(&point->y, 1);
	FIELD_LOAD_SMALL(&point->z, 1);
	FIELD_LOAD_SMALL(&point->t, 0);
}

static inline void _point_base(point_t* point)
{
	do_global_init();
	point->x = base_x;
	point->y = base_y;
	FIELD_LOAD_SMALL(&point->z, 1);
	FIELD_MUL(&point->t, &point->x, &point->y);
}

static inline void _point_double(point_t* out, const point_t* in)
{
	field_t A, B, C, D, E, F, G;
	FIELD_SQR(&A, &in->x);
	FIELD_SQR(&B, &in->y);
	FIELD_SQR(&D, &in->z);
	FIELD_ADD(&C, &D, &D);
	FIELD_NEG(&D, &A);
	FIELD_ADD(&E, &in->x, &in->y);
	FIELD_SQR(&F, &E);
	FIELD_SUB(&G, &F, &A);
	FIELD_SUB(&E, &G, &B);
	FIELD_ADD(&G, &D, &B);
	FIELD_SUB(&F, &G, &C);
	FIELD_SUB(&A, &D, &B);
	FIELD_MUL(&out->x, &E, &F);
	FIELD_MUL(&out->y, &G, &A);
	FIELD_MUL(&out->t, &E, &A);
	FIELD_MUL(&out->z, &F, &G);
}

static inline void _point_neg(point_t* out, const point_t* in)
{
	FIELD_NEG(&out->x, &in->x);
	out->y = in->y;
	out->z = in->z;
	FIELD_NEG(&out->t, &in->t);
}

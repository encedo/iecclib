static int point_set(void* _point, const uint8_t* data)
{
	point_t* point = DO_ALIGN(point_t, _point, POINT_ALIGN_POWER);
	return _point_set(point, data);
}

static void point_get(const void* _point, uint8_t* output)
{
	const point_t* point = DO_ALIGN(const point_t, _point, POINT_ALIGN_POWER);
	_point_get(point, output);
}

static int point_set_coords(void* _point, const void* _x, const void* _y)
{
	point_t* point = DO_ALIGN(point_t, _point, POINT_ALIGN_POWER);
	const field_t* x = DO_ALIGN(const field_t, _x, FIELD_ALIGN_POWER);
	const field_t* y = DO_ALIGN(const field_t, _y, FIELD_ALIGN_POWER);
	return _point_set_coords(point, x, y);
}

static void point_get_coords(const void* _point, void* _x, void* _y)
{
	const point_t* point = DO_ALIGN(const point_t, _point, POINT_ALIGN_POWER);
	field_t* x = DO_ALIGN(field_t, _x, FIELD_ALIGN_POWER);
	field_t* y = DO_ALIGN(field_t, _y, FIELD_ALIGN_POWER);
	_point_get_coords(point, x, y);
}

static void point_add(void* _out, const void* _in1, const void* _in2)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	const point_t* in1 = DO_ALIGN(const point_t, _in1, POINT_ALIGN_POWER);
	const point_t* in2 = DO_ALIGN(const point_t, _in2, POINT_ALIGN_POWER);
	_point_add(out, in1, in2);
}

static void point_double(void* _out, const void* _in)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	const point_t* in = DO_ALIGN(const point_t, _in, POINT_ALIGN_POWER);
	_point_double(out, in);
}

static void point_neg(void* _out, const void* _in)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	const point_t* in = DO_ALIGN(const point_t, _in, POINT_ALIGN_POWER);
	_point_neg(out, in);
}

static void point_copy(void* _out, const void* _in)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	const point_t* in = DO_ALIGN(const point_t, _in, POINT_ALIGN_POWER);
	*out = *in;
}

static void point_zero(void* _point)
{
	point_t* point = DO_ALIGN(point_t, _point, POINT_ALIGN_POWER);
	_point_zero(point);
}

static void point_base(void* _point)
{
	point_t* point = DO_ALIGN(point_t, _point, POINT_ALIGN_POWER);
	_point_base(point);
}

static void point_mul(void* _out, const uint32_t* in1, const void* _in2)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	const point_t* in2 = DO_ALIGN(const point_t, _in2, POINT_ALIGN_POWER);
	_point_mul(out, in1, in2);
}

static void point_mul_base(void* _out, const uint32_t* in1)
{
	point_t* out = DO_ALIGN(point_t, _out, POINT_ALIGN_POWER);
	_point_mul_base(out, in1);
}

static int point_montgomery(void* _output, const void* _point)
{
	field_t* output = DO_ALIGN(field_t, _output, FIELD_ALIGN_POWER);
	const point_t* point = DO_ALIGN(const point_t, _point, POINT_ALIGN_POWER);
	return _point_montgomery(output, point);
}

static int montgomery_mul(void* _output, const uint32_t* mul, const void* _base)
{
	field_t* output = DO_ALIGN(field_t, _output, FIELD_ALIGN_POWER);
	const field_t* base = DO_ALIGN(field_t, _base, FIELD_ALIGN_POWER);
	return _montgomery_mul(output, mul, base);
}


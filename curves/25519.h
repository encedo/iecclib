#ifndef _ecclib_curve25519_h_included_
#define _ecclib_curve25519_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_curve25519 "curve25519"

#define curve25519 FAKE_NOEXPORT(curve25519)

/**
 * The handle of curve25519 function.
 */
extern const struct ecclib_curve curve25519;

#endif

#include "iecclib.h"
#include "3363.h"
#include "fields/3363defs.inc"
#include "utils.h"
#include <stdlib.h>

#define CURVE_NAME NAME_E3363
#define CLOG2L 334
#define RLOG2L 333
#define CURVE_FIELD field_3363
#define PARTIAL_MAX_BITS 704
#define COFACTOR 8
#define SCALAR_WORDS 11
#define POINT_ALIGN_POWER 4
#define FIELD_BITS FE3363_BITS
#define HAS_COORDINATE_T
//Yeah, montgomery constant and edwards K is negative.
#define MONTGOMERY_CONSTANT -11110
#define EDWARDS_CONST -11111
#define EDWARDS_CONST_K (2*EDWARDS_CONST)
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {4};

//Montgomery basepoint is:
//u = 4
//v = 8332547917553245152200590460874618413306360482372197323290404442105423356293100071019928588958189336

//Edwards basepoint is:
//The sign of x can't be resolved.
//x = +-29429332382965038738329217392533655068209823703932876847855112863257110905993011846265332315330424863
//y = 128487149650595282782014831555025757126346546215826747878267730726432613110952777210471886863779333133

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		//The sign of x can't be resolved because the sign of i can't be resolved.
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x1F, 0xB0, 0x0B, 0xA9, 0x38, 0xAB, 0xE4, 0x48,
			0x4F, 0x9A, 0x36, 0x40, 0x7C, 0xA9, 0xE0, 0x92,
			0x98, 0x50, 0x1E, 0x67, 0x21, 0x0A, 0xD9, 0xED,
			0x50, 0xD2, 0xE5, 0x9A, 0x86, 0x16, 0x33, 0x3A,
			0x53, 0xBE, 0xCE, 0x8B, 0xDC, 0x8E, 0x67, 0xDC,
			0xD1, 0x35
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x0D, 0x84, 0x65, 0xE5, 0x8F, 0x94, 0x55, 0x18,
			0x2E, 0x6B, 0xE8, 0x4D, 0x61, 0x68, 0x1A, 0x22,
			0x48, 0x6A, 0x59, 0x45, 0x47, 0x60, 0x7B, 0x9E,
			0x57, 0xCB, 0xF5, 0x48, 0x97, 0x89, 0x8B, 0x02,
			0x98, 0x2F, 0xCA, 0x3E, 0x26, 0x07, 0x69, 0x86,
			0xF9, 0xEA
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD_SMALL(&const_one, 1);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point3363
{
	field_t x;
	field_t y;
	field_t z;
	field_t t;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-extended.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	do_global_init();
	field_t A, B, C, D, E, F, G;

	FIELD_SUB(&B, &in1->y, &in1->x);
	FIELD_SUB(&C, &in2->y, &in2->x);
	FIELD_MUL(&A, &B, &C);
	FIELD_ADD(&D, &in1->y, &in1->x);
	FIELD_ADD(&C, &in2->y, &in2->x);
	FIELD_MUL(&B, &D, &C);
	FIELD_SMUL(&D, &in2->t, -EDWARDS_CONST_K);	//D has wrong sign.
	FIELD_MUL(&C, &in1->t, &D);			//C has wrong sign.
	FIELD_ADD(&E, &in2->z, &in2->z);
	FIELD_MUL(&D, &in1->z, &E);			//D overwitten with vaue of correct sign, C still wrong.
	FIELD_SUB(&E, &B, &A);
	FIELD_ADD(&F, &D, &C);				//C has wrong sign, so change SUB to ADD.
	FIELD_SUB(&G, &D, &C);				//C has wrong sign, so change ADD to SUB.
	FIELD_ADD(&C, &B, &A);				//C overwritten with value of correct sign, all signs correct.
	FIELD_MUL(&out->x, &E, &F);
	FIELD_MUL(&out->y, &G, &C);
	FIELD_MUL(&out->t, &E, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;
	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);
	syndrome |= data[42]&127;  //Excess bits MUST be 0.

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);			//y^2
	FIELD_SUB(&B, &A, &const_one);		//y^2 - 1
	FIELD_SMUL(&C, &A, -EDWARDS_CONST);	//-d*y^2
	FIELD_SUB(&A, &const_one, &C);		//d*y^2 + 1
	FIELD_INV(&C, &A);			//1/(d*y^2 + 1)
	FIELD_MUL(&A, &B, &C);			//(y^2 - 1)/(d*y^2 + 1)
	uint32_t flag = FIELD_SQRT(&B, &A);	//sqrt((y^2 - 1)/(d*y^2 + 1))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	FIELD_MUL(&B, &C, &y);
	FIELD_LOAD_COND(&point->t, &B, flag);
	return flag;
}

static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_INV(&A, &point->x);
	FIELD_MUL(&B, &point->y, &A);
	FIELD_SQR(&A, &B);
	FIELD_NEG(output, &A);
	return 1;
}

//Curve order is:
//17498005798264095394980017816940970922825355447145709836587587134987699588874282995658187611024893957
//200000000000000000000000000000000000000000071415FA9850C0BD6B87F93BAA7B2F95973E9FA805

static const uint32_t high_word_mul[6] = {
	0x40280000, 0xACB9F4FD, 0xDD53D97C, 0xEB5C3FC9, 0xD4C28605, 0x0038A0AF
};
static const uint32_t high_bit_mul[6] = {
	0x3E9FA805, 0x7B2F9597, 0x87F93BAA, 0x50C0BD6B, 0x1415FA98, 0x00000007
};
static const uint32_t curve_order[11] = {
	0x3E9FA805, 0x7B2F9597, 0x87F93BAA, 0x50C0BD6B, 0x1415FA98, 0x00000007, 0x00000000, 0x00000000,
	0x00000000, 0x00000000, 0x00002000
};
static const uint32_t bias1[17] = {
	0x999DA88E, 0x11A28F1B, 0xC5E24FBF, 0x1CA8D5E8, 0xCF5DA468, 0xE7FAD141, 0xD8985EE4, 0x305564F4,
	0x35201353, 0x5CE45C70, 0x000017D2, 0x40280000, 0xACB9F4FD, 0xDD53D97C, 0xEB5C3FC9, 0xD4C28605,
	0x0038A0AF
};

static const uint32_t bias2[12] = {
	0xB3A46E52, 0x3427D660, 0x2F4F2B09, 0xB59B3A7C, 0x5AE883A9, 0x21AC7259, 0x00000003, 0x00000000,
	0x00000000, 0x00000000, 0x2BF54000, 0x00000E28
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[17];
	uint32_t tmp2[17];
	uint32_t tmp3[17];

	//First, split to lower 352 bit and upper 352 bit parts, multiply upper and accumulate. The largest possible
	//result for this is 2^534 (17 words). We need to pad the lower words due to addition routine workings.
	ecclib_bigint_mul(tmp1, input + 11, 11, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	ecclib_bigint_sub(tmp3, bias1, tmp1, 17);
	for(unsigned i = 0; i < 11; i++) tmp2[i] = input[i];
	for(unsigned i = 11; i < 17; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp3, tmp2, 17);

	//Then split the lower 352-bit and upper 182-bit (6 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^364 (12 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp2, tmp1 + 11, 6, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	ecclib_bigint_sub(tmp3, bias2, tmp2, 12);
	for(unsigned i = 0; i < 11; i++) tmp2[i] = tmp1[i];
	for(unsigned i = 11; i < 12; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp2, tmp3, 12);

	//Then split the lower 333-bit and upper 31-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^335. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp1[10] >> 13) | (tmp1[11] << 19);
	tmp1[10] &= (1UL << 13) - 1;
	ecclib_bigint_mul(tmp2, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 11; i++) tmp2[i] = 0;
	ecclib_bigint_sub(tmp3, curve_order, tmp2, 11);
	ecclib_bigint_add(output, tmp1, tmp3, 11);
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[12];
	uint32_t tmp2[12];
	uint32_t tmp3[12];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 11; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[10] >> 13);
	tmp3[10] &= (1UL << 13) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 11; i++) tmp1[i] = 0;
	ecclib_bigint_sub(tmp2, curve_order, tmp1, 11);
	ecclib_bigint_add(tmp1, tmp2, tmp3, 11);
	//Now tmp1 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 10; i < 11; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp1[i] < curve_order[i]);
		gt |= neutral & (tmp1[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 11; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp2, tmp1, tmp3, 11);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, E3363.scalar_octets, tmp2);
}

const static uint8_t _order[] = {
	0x05, 0xA8, 0x9F, 0x3E, 0x97, 0x95, 0x2F, 0x7B, 0xAA, 0x3B, 0xF9, 0x87, 0x6B, 0xBD, 0xC0, 0x50,
	0x98, 0xFA, 0x15, 0x14, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20
};

const struct ecclib_curve E3363 = {
#include "curve-fns.inc"
};

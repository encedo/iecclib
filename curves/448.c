#include "iecclib.h"
#include "448.h"
#include "fields/448defs.inc"
#include "utils.h"
#include "utils2.h"

#define CURVE_NAME NAME_curve448
#define CLOG2L 446
#define RLOG2L 446
#define CURVE_FIELD field_448
#define PARTIAL_MAX_BITS 922
#define COFACTOR 4
#define FLAGS ECCLIB_FLAG_HAS_MONTGOMERY
#define SCALAR_WORDS 15
#define POINT_ALIGN_POWER 4
#define EDWARDS_CONST -39081
#define MONTGOMERY_CONSTANT 39082
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {5};

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x5E, 0xC0, 0x0C, 0xC7, 0x2B, 0xA8, 0x26, 0x26, 0x8E, 0x93, 0x00, 0x8B, 0xE1, 0x80,
			0x3B, 0x43, 0x11, 0x65, 0xB6, 0x2A, 0xF7, 0x1A, 0xAE, 0x12, 0x64, 0xA4, 0xD3, 0xA3,
			0x24, 0xE3, 0x6D, 0xEA, 0x67, 0x17, 0x0F, 0x47, 0x70, 0x65, 0x14, 0x9E, 0xDA, 0x36,
			0xBF, 0x22, 0xA6, 0x15, 0x1D, 0x22, 0xED, 0x0D, 0xED, 0x6B, 0xC6, 0x70, 0x19, 0x4F
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x14, 0xfa, 0x30, 0xf2, 0x5b, 0x79, 0x08, 0x98, 0xad, 0xc8, 0xd7, 0x4e, 0x2c, 0x13,
			0xbd, 0xfd, 0xc4, 0x39, 0x7c, 0xe6, 0x1c, 0xff, 0xd3, 0x3a, 0xd7, 0xc2, 0xa0, 0x05,
			0x1e, 0x9c, 0x78, 0x87, 0x40, 0x98, 0xa3, 0x6c, 0x73, 0x73, 0xea, 0x4b, 0x62, 0xc7,
			0xc9, 0x56, 0x37, 0x20, 0x76, 0x88, 0x24, 0xbc, 0xb6, 0x6e, 0x71, 0x46, 0x3f, 0x69
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD_SMALL(&const_one, 1);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point448
{
	field_t x;
	field_t y;
	field_t z;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-normal.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	field_t A, B, C, D, E, F, G, H;
	FIELD_MUL(&A, &in1->z, &in2->z);
	FIELD_SQR(&B, &A);
	FIELD_MUL(&C, &in1->x, &in2->x);
	FIELD_MUL(&D, &in1->y, &in2->y);
	FIELD_MUL(&F, &C, &D);
	FIELD_SMUL(&E, &F, -EDWARDS_CONST);	//Really -39081, flip sign of E.
	FIELD_ADD(&F, &B, &E);
	FIELD_SUB(&G, &B, &E);
	FIELD_ADD(&B, &in1->x, &in1->y);
	FIELD_ADD(&E, &in2->x, &in2->y);
	FIELD_MUL(&H, &B, &E);
	FIELD_SUB(&B, &H, &C);
	FIELD_SUB(&E, &B, &D);
	FIELD_MUL(&B, &F, &E);
	FIELD_MUL(&out->x, &A, &B);
	FIELD_SUB(&B, &D, &C);
	FIELD_MUL(&C, &G, &B);
	FIELD_MUL(&out->y, &A, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;

	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);
	syndrome |= data[56]&127;  //Excess bits MUST be 0.

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);				//y^2
	FIELD_SUB(&B, &const_one, &A);			//1 - y^2
	FIELD_SMUL(&C, &A, -EDWARDS_CONST);		//-d*y^2
	FIELD_ADD(&A, &C, &const_one);			//-d*y^2 + 1
	FIELD_INV(&C, &A);				//1/(-d*y^2 + 1)
	FIELD_MUL(&A, &B, &C);				//(1 - y^2)/(-d*y^2 + 1)
	uint32_t flag = FIELD_SQRT(&B, &A);		//sqrt((1 - y^2)/(-d*y^2 + 1))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	return flag;
}

static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_INV(&A, &point->x);
	FIELD_MUL(&B, &point->y, &A);
	FIELD_SQR(output, &B);
	return 1;
}

static const uint32_t high_word_mul[8] = {
	0x529eec34, 0x721cf5b5, 0xc8e9c2ab, 0x7a4cf635, 0x44a725bf, 0xeec492d9, 0x0cd77058, 0x00000002
};
static const uint32_t high_bit_mul[7] = {
	0x54a7bb0d, 0xdc873d6d, 0x723a70aa, 0xde933d8d, 0x5129c96f, 0x3bb124b6, 0x8335dc16
};
static const uint32_t curve_order[15] = {
	0xab5844f3, 0x2378c292, 0x8dc58f55, 0x216cc272, 0xaed63690, 0xc44edb49, 0x7cca23e9,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0x3fffffff
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[23];
	uint32_t tmp2[23];
	uint32_t tmp3[23];
	//First, split to lower 448-bit and upper 474-bit parts, multiply upper and accumulate. The largest possible
	//result for this is 2^700 (22 words). We need to pad the lower words due to addition routine workings.
	ecclib_bigint_mul(tmp1, input + 14, 15, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 14; i++) tmp2[i] = input[i];
	for(unsigned i = 14; i < 22; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 22);
	//Then split the lower 448-bit and upper 252-bit (8 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^478 (15 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp1, tmp3 + 14, 8, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 14; i++) tmp2[i] = tmp3[i];
	for(unsigned i = 14; i < 15; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 15);
	//Then split the lower 446-bit and upper 32-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^447. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp3[13] >> 30) | (tmp3[14] << 2);
	tmp3[13] &= (1UL << 30) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 14; i++) tmp1[i] = 0;
	ecclib_bigint_add(output, tmp1, tmp3, 14);
	output[14] = 0;
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[15];
	uint32_t tmp2[15];
	uint32_t tmp3[15];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 15; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[13] >> 30) | (tmp3[14] << 2);
	tmp3[13] &= (1UL << 30) - 1;
	tmp3[14] = 0;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 14; i++) tmp1[i] = 0;
	ecclib_bigint_add(tmp2, tmp1, tmp3, 14);
	//Now tmp2 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 13; i < 14; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp2[i] < curve_order[i]);
		gt |= neutral & (tmp2[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 14; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp1, tmp2, tmp3, 14);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, curve448.scalar_octets, tmp1);
}

const static uint8_t _order[] = {
	0xf3, 0x44, 0x58, 0xab, 0x92, 0xc2, 0x78, 0x23, 0x55, 0x8f, 0xc5, 0x8d, 0x72, 0xc2, 0x6c, 0x21,
	0x90, 0x36, 0xd6, 0xae, 0x49, 0xdb, 0x4e, 0xc4, 0xe9, 0x23, 0xca, 0x7c, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f
};

const struct ecclib_curve curve448 = {
#include "curve-fns.inc"
};

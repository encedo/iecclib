#ifndef _ecclib_curve5211_h_included_
#define _ecclib_curve5211_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_E521 "E-521"

#define E521 FAKE_NOEXPORT(E521)

/**
 * The handle of E521 function.
 */
extern const struct ecclib_curve E521;

#endif

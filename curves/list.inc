#ifdef DO_CC_INCLUDES
#include "25519.h"
#include "3363.h"
#include "38921.h"
#include "41417.h"
#include "448.h"
#include "e521.h"
#else
ALIAS("Edwards25519", NAME_curve25519)
ALIAS("Edwards448", NAME_curve448)
CANDIDATE(curve25519)
CANDIDATE(E3363)
CANDIDATE(Ed38921)
CANDIDATE(curve41417)
CANDIDATE(curve448)
CANDIDATE(E521)
#endif

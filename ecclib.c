#include "iecclib.h"
#include "utils.h"

static void dummy_thread_fn(void* ctx)
{
	(void)ctx;
}

static void(*thread_lock)(void* ctx) = dummy_thread_fn;
static void(*thread_unlock)(void* ctx) = dummy_thread_fn;
static void* thread_ctx = NULL;

void call_thread_lock()
{
	thread_lock(thread_ctx);
}

void call_thread_unlock()
{
	thread_unlock(thread_ctx);
}

void ecclib_set_thread_fns(void(*lock)(void* ctx), void(*unlock)(void* ctx), void* ctx)
{
	thread_lock = lock ? lock : dummy_thread_fn;
	thread_unlock = unlock ? unlock : dummy_thread_fn;
	thread_ctx = ctx;
}

static void clamp_scalar(const struct ecclib_curve* crv, uint32_t* scalar)
{
	uint32_t cofactor = crv->cofactor;
	uint32_t pbits = 0;
	while(cofactor > 1) { pbits++; cofactor >>= 1; }
	pbits += crv->rlog2l - 1;
	scalar[0] &= ~(uint32_t)(crv->cofactor - 1);
	for(unsigned i = (pbits + 32) / 32; i < crv->scalar_elems_single; i++) scalar[i] = 0;
	scalar[pbits/32] &= ((1UL << (pbits%32)) - 1);
	scalar[pbits/32] |= (1UL << (pbits%32));
}

int ecclib_xcurve_base(const struct ecclib_curve* crv, uint8_t* pubkey, const uint8_t* secret)
{
	if(!crv->point_montgomery) return 0;
	uint8_t _pubkey[crv->field->element_size];
	uint8_t _tmp[crv->element_size];
	uint32_t _secret[crv->scalar_elems_single];
	ecclib_bigint_load(_secret, crv->scalar_elems_single, secret, crv->field->storage_octets);
	clamp_scalar(crv, _secret);
	crv->point_mul_base(_tmp, _secret);
	if(crv->point_montgomery(_pubkey, _tmp) <= 0) return 0;
	ecclib_zeroize((uint8_t*)_secret, sizeof(_secret));
	crv->field->get(_pubkey, pubkey);
	return 1;
}

int ecclib_xcurve(const struct ecclib_curve* crv, uint8_t* shared, const uint8_t* secret, const uint8_t* ppubkey)
{
	if(!crv->montgomery_mul) return 0;
	uint8_t _ppubkey[crv->field->element_size];
	uint8_t _shared[crv->field->element_size];
	uint32_t _secret[crv->scalar_elems_single];
	ecclib_bigint_load(_secret, crv->scalar_elems_single, secret, crv->field->storage_octets);
	clamp_scalar(crv, _secret);
	crv->field->set(_ppubkey, ppubkey);
	if(crv->montgomery_mul(_shared, _secret, _ppubkey) <= 0) return 0;
	crv->field->get(_shared, shared);
	ecclib_zeroize((uint8_t*)_secret, sizeof(_secret));
	ecclib_zeroize(_shared, sizeof(_shared));
	return 1;
}

const char* ecclib_field_name(const struct ecclib_field* field)
{
	return field->name;
}

size_t ecclib_field_element_size(const struct ecclib_field* field)
{
	return field->element_size;
}

size_t ecclib_field_field_bits(const struct ecclib_field* field)
{
	return field->field_bits;
}

size_t ecclib_field_storage_octets(const struct ecclib_field* field)
{
	return field->storage_octets;
}

const uint8_t* ecclib_field_field_element_count(const struct ecclib_field* field)
{
	return field->field_element_count;
}

const uint8_t* ecclib_field_field_characteristic(const struct ecclib_field* field)
{
	return field->field_characteristic;
}

size_t ecclib_field_degree(const struct ecclib_field* field)
{
	return field->degree;
}

size_t ecclib_field_degree_stride(const struct ecclib_field* field)
{
	return field->degree_stride;
}

size_t ecclib_field_degree_bits(const struct ecclib_field* field)
{
	return field->degree_bits;
}

void ecclib_field_zero(const struct ecclib_field* field, void* out)
{
	field->zero(out);
}

void ecclib_field_set_int(const struct ecclib_field* field, void* out, uint32_t val)
{
	field->set_int(out, val);
}

void ecclib_field_set(const struct ecclib_field* field, void* out, const uint8_t* val)
{
	field->set(out, val);
}

void ecclib_field_get(const struct ecclib_field* field, const void* in, uint8_t* val)
{
	field->get(in, val);
}

int ecclib_field_check(const struct ecclib_field* field, const uint8_t* val)
{
	return field->check(val);
}

void ecclib_field_add(const struct ecclib_field* field, void* out, const void* in1, const void* in2)
{
	field->add(out, in1, in2);
}

void ecclib_field_sub(const struct ecclib_field* field, void* out, const void* in1, const void* in2)
{
	field->sub(out, in1, in2);
}

void ecclib_field_mul(const struct ecclib_field* field, void* out, const void* in1, const void* in2)
{
	field->mul(out, in1, in2);
}

void ecclib_field_smul(const struct ecclib_field* field, void* out, const void* in1, uint32_t in2)
{
	field->smul(out, in1, in2);
}

void ecclib_field_neg(const struct ecclib_field* field, void* out, const void* in)
{
	field->neg(out, in);
}

void ecclib_field_sqr(const struct ecclib_field* field, void* out, const void* in)
{
	field->sqr(out, in);
}

void ecclib_field_inv(const struct ecclib_field* field, void* out, const void* in)
{
	field->inv(out, in);
}

int ecclib_field_sqrt(const struct ecclib_field* field, void* out, const void* in)
{
	return field->sqrt(out, in);
}

void ecclib_field_copy(const struct ecclib_field* field, void* out, const void* in)
{
	field->copy(out, in);
}

void ecclib_field_load_cond(const struct ecclib_field* field, void* out, const void* in, uint32_t flag)
{
	field->load_cond(out, in, flag & 1);
}

void ecclib_field_select(const struct ecclib_field* field, void* out, const void* in1, const void* in2,
	uint32_t flag)
{
	field->select(out, in1, in2, flag & 1);
}

void ecclib_field_cswap(const struct ecclib_field* field, void* x1, void* x2, uint32_t flag)
{
	field->cswap(x1, x2, flag & 1);
}

const char* ecclib_curve_name(const struct ecclib_curve* curve)
{
	return curve->name;
}

const struct ecclib_field* ecclib_curve_field(const struct ecclib_curve* curve)
{
	return curve->field;
}

size_t ecclib_curve_storage_octets(const struct ecclib_curve* curve)
{
	return curve->storage_octets;
}

size_t ecclib_curve_element_size(const struct ecclib_curve* curve)
{
	return curve->element_size;
}

const uint8_t* ecclib_curve_order(const struct ecclib_curve* curve)
{
	return curve->order;
}

size_t ecclib_curve_clog2l(const struct ecclib_curve* curve)
{
	return curve->clog2l;
}

size_t ecclib_curve_rlog2l(const struct ecclib_curve* curve)
{
	return curve->rlog2l;
}

size_t ecclib_curve_partial_max_bits(const struct ecclib_curve* curve)
{
	return curve->partial_max_bits;
}

size_t ecclib_curve_scalar_elems_single(const struct ecclib_curve* curve)
{
	return curve->scalar_elems_single;
}

size_t ecclib_curve_scalar_elems_double(const struct ecclib_curve* curve)
{
	return curve->scalar_elems_double;
}

size_t ecclib_curve_scalar_octets(const struct ecclib_curve* curve)
{
	return curve->scalar_octets;
}

uint32_t ecclib_curve_cofactor(const struct ecclib_curve* curve)
{
	return curve->cofactor;
}

uint32_t ecclib_curve_flags(const struct ecclib_curve* curve)
{
	return curve->flags;
}

const uint8_t* ecclib_curve_montgomery_base(const struct ecclib_curve* curve)
{
	return curve->montgomery_base;
}

void ecclib_curve_reduce_partial(const struct ecclib_curve* curve, uint32_t* output, const uint32_t* input)
{
	curve->reduce_partial(output, input);
}

void ecclib_curve_reduce_full(const struct ecclib_curve* curve, uint8_t* output, const uint32_t* input)
{
	curve->reduce_full(output, input);
}

void ecclib_curve_point_zero(const struct ecclib_curve* curve, void* point)
{
	curve->point_zero(point);
}

void ecclib_curve_point_base(const struct ecclib_curve* curve, void* point)
{
	curve->point_base(point);
}

int ecclib_curve_point_set(const struct ecclib_curve* curve, void* point, const uint8_t* data)
{
	return curve->point_set(point, data);
}

void ecclib_curve_point_get(const struct ecclib_curve* curve, const void* point, uint8_t* output)
{
	curve->point_get(point, output);
}

void ecclib_curve_point_add(const struct ecclib_curve* curve, void* out, const void* in1, const void* in2)
{
	curve->point_add(out, in1, in2);
}

void ecclib_curve_point_double(const struct ecclib_curve* curve, void* out, const void* in)
{
	curve->point_double(out, in);
}

void ecclib_curve_point_mul_base(const struct ecclib_curve* curve, void* out, const uint32_t* mul)
{
	curve->point_mul_base(out, mul);
}

void ecclib_curve_point_mul(const struct ecclib_curve* curve, void* out, const uint32_t* mul, const void* in)
{
	curve->point_mul(out, mul, in);
}

void ecclib_curve_point_neg(const struct ecclib_curve* curve, void* out, const void* in)
{
	curve->point_neg(out, in);
}

void ecclib_curve_point_copy(const struct ecclib_curve* curve, void* out, const void* in)
{
	curve->point_copy(out, in);
}

void ecclib_curve_point_get_coords(const struct ecclib_curve* curve, const void* point, void* x, void* y)
{
	curve->point_get_coords(point, x, y);
}

int ecclib_curve_point_set_coords(const struct ecclib_curve* curve, void* point, const void* x, const void* y)
{
	return curve->point_set_coords(point, x, y);
}

int ecclib_curve_point_montgomery(const struct ecclib_curve* curve, void* output, const void* point)
{
	if(!curve->point_montgomery) return 0;
	return curve->point_montgomery(output, point);
}

int ecclib_curve_montgomery_mul(const struct ecclib_curve* curve, void* output, const uint32_t* mul,
	const void* base)
{
	if(!curve->montgomery_mul) return 0;
	return curve->montgomery_mul(output, mul, base);
}

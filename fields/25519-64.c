#include "25519.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 51) - 1;
static const unsigned eshift = 51;
static const uint64_t fconst = 19;

//The modulus as words
static const uint64_t modulus64[5] = {
	0x7FFFFFFFFFFED, 0x7FFFFFFFFFFFF, 0x7FFFFFFFFFFFF, 0x7FFFFFFFFFFFF, 0x7FFFFFFFFFFFF
};

void fe25519_load(struct fe25519* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 5; i++) {
		while(shift < eshift) {
			if(idx == 32) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
}

static void reduce(uint64_t out[5], const uint64_t in[5])
{
	uint64_t carry = fconst * (in[4] >> eshift);
	for(unsigned i = 0; i < 5; i++) out[i] = in[i];
	out[4] = in[4] & emask;
	for(unsigned i = 0; i < 5; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[4] = out[4] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[5] = {0};
	for(unsigned i = 4; i < 5; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 5; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 5; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe25519_store(uint8_t* out, const struct fe25519* elem)
{
	uint64_t y[5];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 5; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 32) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
	if(shift < 8 && idx < 32) {
		out[idx] = tmp;
	}
}

void fe25519_zero(struct fe25519* out)
{
	for(unsigned i = 0; i < 5; i++) out->x[i] = 0;
}

void fe25519_load_small(struct fe25519* out, uint32_t num)
{
	for(unsigned i = 0; i < 5; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe25519_neg(struct fe25519* out, const struct fe25519* in)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) - in->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 5; i++) {
		uint64_t r = 4 * emask - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe25519_sub(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) + in1->x[0] - in2->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 5; i++) {
		uint64_t r = 4 * emask + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe25519_add(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 5; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += fconst * carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe25519_smul(struct fe25519* out, const struct fe25519* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 5; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + fconst * carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
}

void fe25519_mul(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	__uint128_t W=0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4];
	uint64_t c1 = fconst * b1, c2 = fconst * b2, c3 = fconst * b3, c4 = fconst * b4;

	W = W + WM(a0, b0) + WM(a1, c4) + WM(a2, c3) + WM(a3, c2) + WM(a4, c1);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, b1) + WM(a1, b0) + WM(a2, c4) + WM(a3, c3) + WM(a4, c2);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, c4) + WM(a4, c3);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, c4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, b4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a4, b0);
	out->x[4] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask; W >>= eshift;
	out->x[1] += W;
}

void fe25519_sqr(struct fe25519* out, const struct fe25519* in)
{
	__uint128_t W=0;
	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4];
	uint64_t b1 = fconst * a1, b2 = fconst * a2, b3 = fconst * a3, b4 = fconst * a4;
	uint64_t c1 = a1 << 1, c2 = a2 << 1, c3 = a3 << 1, c4 = a4 << 1;

	W = W + WM(a0, a0) + WM(b1, c4) + WM(b2, c3);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, c1) + WM(b2, c4) + WM(b3, a3);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, c2) + WM(a1, a1) + WM(b3, c4);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, c3) + WM(a1, c2) + WM(b4, a4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, c4) + WM(a1, c3) + WM(a2, a2);
	out->x[4] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask; W >>= eshift;
	out->x[1] += W;
}

void fe25519_load_cond(struct fe25519* out, const struct fe25519* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 5; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe25519_select(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 5; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe25519_cswap(struct fe25519* x0, struct fe25519* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 5; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

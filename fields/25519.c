#include "25519defs.inc"
#include "iecclib.h"
#ifdef DEFAULT_F25519
//in is assumed to be <2^533
static void reduce_after_mul(uint32_t out[9], const uint32_t in[18])
{
	uint32_t tmp[17];
	uint32_t tmpl[9];
	uint32_t tmph[10];
	uint32_t hmul = 38;

	//Multiply high part of in by 38 and add to low part.
	for(unsigned i = 0; i < 8; i++) tmpl[i] = in[i];
	tmpl[8] = 0;	//This is needed for padding.
	ecclib_bigint_mul(tmph, in + 8, 9, &hmul, 1);
	ecclib_bigint_add(tmp, tmpl, tmph, 9);
	//Now tmp is at most 159383556*2^255, so carry will fit to 32 bits.
	uint64_t carry = ((tmp[7] >> 31) | (tmp[8] << 1)) * 19;
	for(unsigned i = 0; i < 7; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[7] = carry + (tmp[7] & 0x7FFFFFFF);
	out[8] = 0;
}

//in is assumed to be <226050910*2^255
static void reduce_after_add(uint32_t out[9], const uint32_t in[9])
{
	uint64_t carry = ((in[7] >> 31) | (in[8] << 1)) * 19;
	for(unsigned i = 0; i < 7; i++) {
		carry += in[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[7] = carry + (in[7] & 0x7FFFFFFF);
	out[8] = 0;
}

//65536 * modulus
static const uint32_t sub_bias[9] = {
	0xFFED0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x7FFF
};

//The modulus as words
static const uint32_t modulus32[9] = {
	0xFFFFFFED, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0
};

void fe25519_load(struct fe25519* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 9; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 32; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
	out->x[7] &= 0x7FFFFFFF;	//Mask high bit.
}

void fe25519_store(uint8_t* out, const struct fe25519* elem)
{
	uint32_t tmp[9];
	uint32_t tmp3[9];
	uint32_t sub[9];
	uint64_t carry = ((elem->x[7] >> 31) | (elem->x[8] << 1)) * 19;
	for(unsigned i = 0; i < 7; i++) {
		carry += elem->x[i];
		tmp[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	tmp[7] = carry + (elem->x[7] & 0x7FFFFFFF);
	tmp[8] = 0;

	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 7; i < 8; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 9; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp, sub, 9);
	//Now, write this to bytes
	for(unsigned i = 0; i < 32; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe25519_zero(struct fe25519* out)
{
	for(unsigned i = 0; i < 9; i++) out->x[i] = 0;
}

void fe25519_load_small(struct fe25519* out, uint32_t num)
{
	for(unsigned i = 0; i < 9; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe25519_neg(struct fe25519* out, const struct fe25519* in)
{
	uint32_t result[9];
	ecclib_bigint_sub(result, sub_bias, in->x, 9);
	reduce_after_add(out->x, result);
}

void fe25519_sub(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	uint32_t tmp[9];
	uint32_t result[9];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 9);
	ecclib_bigint_sub(result, tmp, in2->x, 9);
	reduce_after_add(out->x, result);
}

void fe25519_add(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	uint32_t result[9];
	ecclib_bigint_add(result, in1->x, in2->x, 9);
	reduce_after_add(out->x, result);
}

void fe25519_smul(struct fe25519* out, const struct fe25519* in1, uint32_t in2)
{
	uint32_t result[10];
	ecclib_bigint_mul(result, in1->x, 9, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe25519_mul(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2)
{
	uint32_t result[18];
	ecclib_bigint_mul(result, in1->x, 9, in2->x, 9);
	reduce_after_mul(out->x, result);
}

void fe25519_sqr(struct fe25519* out, const struct fe25519* in)
{
	uint32_t result[18];
	ecclib_bigint_mul(result, in->x, 9, in->x, 9);
	reduce_after_mul(out->x, result);
}

void fe25519_load_cond(struct fe25519* out, const struct fe25519* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 9; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe25519_select(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 9; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe25519_cswap(struct fe25519* x0, struct fe25519* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 9; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "25519-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"

//The modulus as bytes
static const uint8_t modulus[49] = {
	0xEB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x1F
};

int fe38921_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 48; i < 49; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe38921_sqrn_even(struct fe38921* out, const struct fe38921* in, unsigned count)
{
	struct fe38921 tmp1, tmp2;
	fe38921_sqr(&tmp1, in);
	for(unsigned i = 0; i < (count-2)/2; i++) {
		fe38921_sqr(&tmp2, &tmp1);
		fe38921_sqr(&tmp1, &tmp2);
	}
	fe38921_sqr(out, &tmp1);
}

static void fe38921_sqrn3(struct fe38921* out, const struct fe38921* in)
{
	struct fe38921 tmp1, tmp2;
	fe38921_sqr(&tmp1, in);
	fe38921_sqr(&tmp2, &tmp1);
	fe38921_sqr(out, &tmp2);
}

int fe38921_sqrt(struct fe38921* out, const struct fe38921* in)
{
	//Raise to power of 2^387-5
	struct fe38921 r, y, g, b;
	fe38921_sqr(&r, in);			//2
	fe38921_mul(&y, &r, in);		//3
	fe38921_sqr(&r, &y);			//6
	fe38921_mul(&g, &r, in);		//7
	fe38921_sqrn3(&r, &g);			//2^6-2^3
	fe38921_mul(&b, &r, &g);		//2^6-1
	fe38921_sqrn_even(&g, &b, 6);		//2^12-2^6
	fe38921_mul(&r, &g, &b);		//2^12-1
	fe38921_sqrn_even(&g, &r, 12);		//2^24-2^12
	fe38921_mul(&b, &g, &r);		//2^24-1
	fe38921_sqrn_even(&r, &b, 24);		//2^48-2^24
	fe38921_mul(&g, &r, &b);		//2^48-1
	fe38921_sqrn_even(&r, &g, 48);		//2^96-2^48
	fe38921_mul(&b, &r, &g);		//2^96-1
	fe38921_sqrn_even(&g, &b, 96);		//2^192-2^96
	fe38921_mul(&r, &g, &b);		//2^192-1
	fe38921_sqrn_even(&b, &r, 192);		//2^384-2^192
	fe38921_mul(&g, &b, &r);		//2^384-1
	fe38921_sqrn3(&r, &g);			//2^387-8
	fe38921_mul(&g, &r, &y);		//2^387-5
	uint8_t check1[49];
	uint8_t check2[49];
	fe38921_sqr(&r, &g);		//check value.
	fe38921_store(check1, in);
	fe38921_store(check2, &r);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 49; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe38921_load_cond(out, &g, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe38921_inv(struct fe38921* out, const struct fe38921* in)
{
	//Raise to power of 2^389-23
	struct fe38921 r, g, b;
	fe38921_sqr(&r, in);			//2
	fe38921_mul(&g, &r, in);		//3
	fe38921_sqr(&r, &g);			//6
	fe38921_mul(&g, &r, in);		//7
	fe38921_sqrn3(&r, &g);			//2^6-2^3
	fe38921_mul(&b, &r, &g);		//2^6-1
	fe38921_sqrn_even(&g, &b, 6);		//2^12-2^6
	fe38921_mul(&r, &g, &b);		//2^12-1
	fe38921_sqrn_even(&g, &r, 12);		//2^24-2^12
	fe38921_mul(&b, &g, &r);		//2^24-1
	fe38921_sqrn_even(&r, &b, 24);		//2^48-2^24
	fe38921_mul(&g, &r, &b);		//2^48-1
	fe38921_sqrn_even(&r, &g, 48);		//2^96-2^48
	fe38921_mul(&b, &r, &g);		//2^96-1
	fe38921_sqrn_even(&g, &b, 96);		//2^192-2^96
	fe38921_mul(&r, &g, &b);		//2^192-1
	fe38921_sqrn_even(&b, &r, 192);		//2^384-2^192
	fe38921_mul(&g, &b, &r);		//2^384-1
	fe38921_sqrn_even(&r, &g, 2);		//2^386-4
	fe38921_mul(&g, &r, in);		//2^386-3
	fe38921_sqrn3(&r, &g);			//2^389-24
	fe38921_mul(out, &r, in);		//2^389-23
}

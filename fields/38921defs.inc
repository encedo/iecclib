#include "38921.h"
#include "utils2.h"
typedef struct fe38921 field_t;
#define FIELD_NAME field_38921
#define FIELD_REAL_NAME NAME_field_38921
#define FIELD_BITS FE38921_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE38921_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE38921_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe38921) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe38921_zero(X)
#define FIELD_CHECK(X) fe38921_check(X)
#define FIELD_LOAD(X, Y) fe38921_load(X, Y)
#define FIELD_STORE(X, Y) fe38921_store(X, Y)
#define FIELD_SQR(X, Y) fe38921_sqr(X, Y)
#define FIELD_INV(X, Y) fe38921_inv(X, Y)
#define FIELD_NEG(X, Y) fe38921_neg(X, Y)
#define FIELD_SQRT(X, Y) fe38921_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe38921_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe38921_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe38921_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe38921_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe38921_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe38921_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe38921_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe38921_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)

#include "41417defs.inc"
#include "iecclib.h"

#ifdef DEFAULT_F41417
//in is assumed to be <2^851
static void reduce_after_mul(uint32_t out[14], const uint32_t in[28])
{
	uint32_t tmp[15];
	uint32_t tmpl[15];
	uint32_t tmph[15];
	uint32_t hmul = 68;

	//Multiply high part of in by 68 and add to low part.
	for(unsigned i = 0; i < 13; i++) tmpl[i] = in[i];
	tmpl[13] = 0;	//This is needed for padding.
	ecclib_bigint_mul(tmph, in + 13, 14, &hmul, 1);
	ecclib_bigint_add(tmp, tmpl, tmph, 14);
	//Now tmp is at most 252645135*2^414, so carry will fit to 32 bits.
	uint64_t carry = ((tmp[12] >> 30) | (tmp[13] << 2)) * 17;
	for(unsigned i = 0; i < 12; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[12] = carry + (tmp[12] & 0x3FFFFFFF);
	out[13] = 0;
}

//in is assumed to be <252645135*2^414
static void reduce_after_add(uint32_t out[14], const uint32_t in[14])
{
	uint64_t carry = ((in[12] >> 30) | (in[13] << 2)) * 17;
	for(unsigned i = 0; i < 12; i++) {
		carry += in[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[12] = carry + (in[12] & 0x3FFFFFFF);
	out[13] = 0;
}

//65536 * modulus
static const uint32_t sub_bias[14] = {
	0xFFEF0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00003FFF
};

//The modulus as words
static const uint32_t modulus32[13] = {
	0xFFFFFFEF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x3FFFFFFF
};

void fe41417_load(struct fe41417* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 14; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 52; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
	out->x[12] &= 0x3FFFFFFF;	//Mask high bit.
	out->x[13] = 0;
}

void fe41417_store(uint8_t* out, const struct fe41417* elem)
{
	uint32_t tmp[13];
	uint32_t tmp3[13];
	uint32_t sub[13];
	uint64_t carry = ((elem->x[12] >> 30) | (elem->x[13] << 2)) * 17;
	for(unsigned i = 0; i < 12; i++) {
		carry += elem->x[i];
		tmp[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	tmp[12] = carry + (elem->x[12] & 0x3FFFFFFF);

	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 12; i < 13; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 13; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp, sub, 13);
	//Now, write this to bytes
	for(unsigned i = 0; i < 52; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe41417_zero(struct fe41417* out)
{
	for(unsigned i = 0; i < 14; i++) out->x[i] = 0;
}

void fe41417_load_small(struct fe41417* out, uint32_t num)
{
	for(unsigned i = 0; i < 14; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe41417_neg(struct fe41417* out, const struct fe41417* in)
{
	uint32_t result[14];
	ecclib_bigint_sub(result, sub_bias, in->x, 14);
	reduce_after_add(out->x, result);
}

void fe41417_sub(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	uint32_t tmp[14];
	uint32_t result[14];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 14);
	ecclib_bigint_sub(result, tmp, in2->x, 14);
	reduce_after_add(out->x, result);
}

void fe41417_add(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	uint32_t result[14];
	ecclib_bigint_add(result, in1->x, in2->x, 14);
	reduce_after_add(out->x, result);
}

void fe41417_smul(struct fe41417* out, const struct fe41417* in1, uint32_t in2)
{
	uint32_t result[15];
	ecclib_bigint_mul(result, in1->x, 14, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe41417_mul(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	uint32_t result[28];
	ecclib_bigint_mul(result, in1->x, 14, in2->x, 14);
	reduce_after_mul(out->x, result);
}

void fe41417_sqr(struct fe41417* out, const struct fe41417* in)
{
	uint32_t result[28];
	ecclib_bigint_mul(result, in->x, 14, in->x, 14);
	reduce_after_mul(out->x, result);
}

void fe41417_load_cond(struct fe41417* out, const struct fe41417* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 14; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe41417_select(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 14; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe41417_cswap(struct fe41417* x0, struct fe41417* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 14; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "41417-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"

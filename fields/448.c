#include "448defs.inc"
#include "iecclib.h"
#include <stdlib.h>

#ifdef DEFAULT_F448
//in is assumed to be <2^926
static void reduce_after_mul(uint32_t out[15], const uint32_t in[30])
{
	uint64_t tmp[29];
	uint64_t tmp2[22];
	for(unsigned i = 0; i < 29; i++) tmp[i] = in[i];
	//Now in represents at most 2^926
	for(unsigned i = 0; i < 14; i++) tmp2[i] = in[i];
	for(unsigned i = 14; i < 22; i++) tmp2[i] = 0;
	for(unsigned i = 0; i < 15; i++) {
		tmp2[i] += in[i+14];
		tmp2[i+7] += in[i+14];
	}
	//Now tmp2 represents at most 2^703
	for(unsigned i = 0; i < 14; i++) tmp[i] = tmp2[i];
	for(unsigned i = 14; i < 15; i++) tmp[i] = 0;
	for(unsigned i = 0; i < 8; i++) {
		tmp[i] += tmp2[i+14];
		tmp[i+7] += tmp2[i+14];
	}
	//Now tmp represents at most 2^480.
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	//Now tmp represents at most 2^449, resolve carries.
	uint64_t carry = 0;
	for(unsigned i = 0; i < 15; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
}

//in is assumed to be <2^480
static void reduce_after_add(uint32_t out[15], const uint32_t in[15])
{
	uint64_t tmp[15];
	for(unsigned i = 0; i < 15; i++) tmp[i] = in[i];
	//Now tmp represents at most 2^480.
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	//Now tmp represents at most 2^449, resolve carries.
	uint64_t carry = 0;
	for(unsigned i = 0; i < 15; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
}

//65536 * modulus
static const uint32_t sub_bias[15] = {
	0xFFFF0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFEFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x0000FFFF
};

//The modulus as words
static const uint32_t modulus32[15] = {
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x00000000
};

void fe448_store(uint8_t* out, const struct fe448* elem)
{
	//We need to perform full reduction.
	uint64_t tmp[15];
	uint32_t tmp2[15];
	uint32_t tmp3[15];
	uint32_t sub[15];
	for(unsigned i = 0; i < 15; i++) tmp[i] = elem->x[i];
	//Now tmp represents at most 2^480.
	tmp[0] += tmp[14];
	tmp[7] += tmp[14];
	tmp[14] = 0;
	//Now tmp represents at most 2^449, resolve carries.
	uint64_t carry = 0;
	for(unsigned i = 0; i < 15; i++) {
		carry += tmp[i];
		tmp2[i] = carry;
		carry >>= 32;
	}
	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 14; i < 15; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 15; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp2, sub, 15);
	//Now, write this to bytes
	for(unsigned i = 0; i < 56; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe448_load(struct fe448* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 15; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 56; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
}

void fe448_zero(struct fe448* out)
{
	for(unsigned i = 0; i < 15; i++) out->x[i] = 0;
}

void fe448_load_small(struct fe448* out, uint32_t num)
{
	for(unsigned i = 0; i < 15; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe448_neg(struct fe448* out, const struct fe448* in)
{
	uint32_t result[15];
	ecclib_bigint_sub(result, sub_bias, in->x, 15);
	reduce_after_add(out->x, result);
}

void fe448_sub(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t tmp[15];
	uint32_t result[15];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 15);
	ecclib_bigint_sub(result, tmp, in2->x, 15);
	reduce_after_add(out->x, result);
}

void fe448_add(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t result[15];
	ecclib_bigint_add(result, in1->x, in2->x, 15);
	reduce_after_add(out->x, result);
}

void fe448_smul(struct fe448* out, const struct fe448* in1, uint32_t in2)
{
	uint32_t result[16];
	ecclib_bigint_mul(result, in1->x, 15, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe448_mul(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint32_t result[30];
	ecclib_bigint_mul(result, in1->x, 15, in2->x, 15);
	reduce_after_mul(out->x, result);
}

void fe448_sqr(struct fe448* out, const struct fe448* in)
{
	uint32_t result[30];
	ecclib_bigint_mul(result, in->x, 15, in->x, 15);
	reduce_after_mul(out->x, result);
}

void fe448_load_cond(struct fe448* out, const struct fe448* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 15; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe448_select(struct fe448* out, const struct fe448* in1, const struct fe448* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 15; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe448_cswap(struct fe448* x0, struct fe448* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 15; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "448-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"

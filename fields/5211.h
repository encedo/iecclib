#ifndef _ecclib_field_5211_h_included_
#define _ecclib_field_5211_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"

struct fe5211
{
#ifdef USE_64BIT_MATH
	uint64_t x[9];
#else
#define DEFAULT_F5211
	uint32_t x[17];
#endif
};

#define NAME_field_5211 "2^521-1"
#define FE5211_BITS 521
#define FE5211_STORE_SIZE BITS_TO_OCTETS(FE5211_BITS)
#define FE5211_ALIGN_POWER 4

#define field_5211 FAKE_NOEXPORT(field_5211)
#define fe5211_add FAKE_NOEXPORT(fe5211_add)
#define fe5211_check FAKE_NOEXPORT(fe5211_check)
#define fe5211_cswap FAKE_NOEXPORT(fe5211_cswap)
#define fe5211_inv FAKE_NOEXPORT(fe5211_inv)
#define fe5211_load FAKE_NOEXPORT(fe5211_load)
#define fe5211_load_cond FAKE_NOEXPORT(fe5211_load_cond)
#define fe5211_load_small FAKE_NOEXPORT(fe5211_load_small)
#define fe5211_mul FAKE_NOEXPORT(fe5211_mul)
#define fe5211_neg FAKE_NOEXPORT(fe5211_neg)
#define fe5211_select FAKE_NOEXPORT(fe5211_select)
#define fe5211_smul FAKE_NOEXPORT(fe5211_smul)
#define fe5211_sqr FAKE_NOEXPORT(fe5211_sqr)
#define fe5211_sqrt FAKE_NOEXPORT(fe5211_sqrt)
#define fe5211_store FAKE_NOEXPORT(fe5211_store)
#define fe5211_sub FAKE_NOEXPORT(fe5211_sub)
#define fe5211_zero FAKE_NOEXPORT(fe5211_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe5211_zero(struct fe5211* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe5211_load_small(struct fe5211* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 32 octets.
 */
void fe5211_load(struct fe5211* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^521-1.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 66 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe5211_check(const uint8_t* num);						//-1 if outside range.

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 32 octets.
 * \param elem The element to store.
 */
void fe5211_store(uint8_t* out, const struct fe5211* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe5211_add(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe5211_sub(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe5211_mul(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe5211_smul(struct fe5211* out, const struct fe5211* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe5211_neg(struct fe5211* out, const struct fe5211* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe5211_sqr(struct fe5211* out, const struct fe5211* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe5211_sqrt(struct fe5211* out, const struct fe5211* in);		//-1 on failure.

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe5211_inv(struct fe5211* out, const struct fe5211* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe5211_load_cond(struct fe5211* out, const struct fe5211* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe5211_select(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe5211_cswap(struct fe5211* x0, struct fe5211* x1, uint32_t flag);

extern const struct ecclib_field field_5211;

#ifdef __cplusplus
}
#endif


#endif

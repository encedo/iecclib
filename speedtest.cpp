#include "iecclib.h"
#include <cstdio>

#define ROUNDS 1000000
#define ROUNDS_LOW 10000

uint64_t tsc()
{
	uint32_t a, b;
	asm volatile("rdtsc" : "=a"(a), "=d"(b));
	return ((uint64_t)b << 32) | a;
}


int main()
{
	uint32_t inbuffer[512];
	uint32_t inbuffer2[512];
	uint32_t outbuffer[512];
	uint32_t outbuffer2[512];
	uint8_t hbuffer[512];
	uint8_t rbuffer[512];
	uint8_t abuffer[512];
	uint8_t buffer1[4096];
	uint8_t buffer2[4096];

	const char* const * curves = ecclib_curve_list();
	uint64_t ts1, ts2;
	for(size_t i = 0; curves[i]; i++) {
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name(curves[i]);
		if(!crv) continue;
		ts1 = tsc();
		for(unsigned i = 0; i < ROUNDS; i++) {
			ecclib_bigint_load(inbuffer, crv->scalar_elems_double, hbuffer, 2 * crv->storage_octets);
			ecclib_bigint_load(inbuffer2, crv->scalar_elems_single, abuffer, crv->storage_octets);
			crv->reduce_partial(outbuffer, inbuffer);
			ecclib_bigint_mul(outbuffer2, outbuffer, crv->scalar_elems_single, inbuffer2,
				crv->scalar_elems_single);
			crv->reduce_partial(outbuffer, outbuffer2);
			ecclib_bigint_load(inbuffer, crv->scalar_elems_double, rbuffer, 2 * crv->storage_octets);
			crv->reduce_partial(inbuffer2, inbuffer);
			ecclib_bigint_add(outbuffer2, inbuffer2, outbuffer, crv->scalar_elems_single);
			crv->reduce_full(buffer1, outbuffer2);
		}
		ts2 = tsc();
		fprintf(stdout, "%s s=r+ha: %f\n", crv->name, (double)(ts2 - ts1) / ROUNDS);
		ts1 = tsc();
		for(unsigned i = 0; i < ROUNDS_LOW; i++) {
			crv->point_mul_base(buffer1, inbuffer2);
		}
		ts2 = tsc();
		fprintf(stdout, "%s fixedmul: %f\n", crv->name, (double)(ts2 - ts1) / ROUNDS_LOW);
		ts1 = tsc();
		for(unsigned i = 0; i < ROUNDS_LOW; i++) {
			crv->point_mul(buffer1, inbuffer2, buffer2);
		}
		ts2 = tsc();
		fprintf(stdout, "%s varmul: %f\n", crv->name, (double)(ts2 - ts1) / ROUNDS_LOW);
	}
	return 0;
}

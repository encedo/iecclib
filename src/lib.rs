#![allow(improper_ctypes)]
extern crate libc;
use std::iter::repeat;
use std::slice::from_raw_parts;
use std::str::from_utf8_unchecked;
use std::fmt::{Debug, Formatter, Error as FmtError};
use std::sync::{Once, ONCE_INIT, Mutex, Condvar};
use libc::{c_int, c_void, size_t, c_char};
#[cfg(test)]
mod tests;

const MAX_FIELDS: usize = 16;
const MAX_FIELD_NAME: usize = 64;
static mut FIELD_NAME_LIST: [&'static str; MAX_FIELDS] = [""; MAX_FIELDS];
static mut FIELD_NAME_LIST_LEN: usize = 0usize;
const MAX_CURVES: usize = 24;
const MAX_CURVE_NAME: usize = 64;
static mut CURVE_NAME_LIST: [&'static str; MAX_CURVES] = [""; MAX_CURVES];
static mut CURVE_NAME_LIST_LEN: usize = 0usize;

///Flag: The curve has Montgomery form.
pub const FLAG_HAS_MONTGOMERY: u32 = 1;
///Flag: The curve has Montgomery form, but it is relatively slow.
pub const FLAG_MONTGOMERY_SLOW: u32 = 2;


///A field
#[repr(C)]
pub struct Field {}

///A field element
#[derive(Debug)]
pub struct FieldElement
{
	field: &'static Field,
	storage: Vec<u8>
}

///A curve
#[repr(C)]
pub struct Curve {}


impl Debug for Field
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<Field '{}'>", self.name())
	}
}

impl Clone for FieldElement
{
	fn clone(&self) -> FieldElement
	{
		let mut ret = FieldElement{field:self.field, storage:repeat(0u8).take(self.field.element_size()).
			collect()};
		ret.clone_from(self);
		ret
	}
	fn clone_from(&mut self, src: &FieldElement)
	{
		let needed = src.field.element_size();
		if self.storage.len() < needed {
			self.storage = repeat(0u8).take(needed).collect();
		}
		self.field = src.field;
		unsafe { ecclib_field_copy(src.field._low(), self.storage.as_mut_ptr() as *mut c_void,
			src.storage.as_ptr() as *const c_void) };
	}
}

impl PartialEq for Field
{
	fn eq(&self, other: &Field) -> bool
	{
		self._low() == other._low()
	}
}

impl Eq for Field {}

impl Debug for Curve
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<Curve '{}'>", self.name())
	}
}

impl PartialEq for Curve
{
	fn eq(&self, other: &Curve) -> bool
	{
		self._low() == other._low()
	}
}

impl Eq for Curve {}

///A curve point
#[derive(Debug)]
pub struct Point
{
	curve: &'static Curve,
	storage: Vec<u8>
}

impl Clone for Point
{
	fn clone(&self) -> Point
	{
		let mut ret = Point{curve:self.curve, storage:repeat(0u8).take(self.curve.element_size()).
			collect()};
		ret.clone_from(self);
		ret
	}
	fn clone_from(&mut self, src: &Point)
	{
		let needed = src.curve.element_size();
		if self.storage.len() < needed {
			self.storage = repeat(0u8).take(needed).collect();
		}
		self.curve = src.curve;
		unsafe { ecclib_curve_point_copy(src.curve._low(), self.storage.as_mut_ptr() as *mut c_void,
			src.storage.as_ptr() as *const c_void) };
	}
}


extern
{
	fn ecclib_bigint_add(out: *mut u32, in1: *const u32, in2: *const u32, words: size_t);
	fn ecclib_bigint_sub(out: *mut u32, in1: *const u32, in2: *const u32, words: size_t);
	fn ecclib_bigint_mul(out: *mut u32, in1: *const u32, words1: size_t, in2: *const u32, words2: size_t);
	fn ecclib_bigint_load(out: *mut u32, outwords: size_t, in1: *const u8, inoctets: size_t);
	fn ecclib_bigint_store(out: *mut u8, outoctets: size_t, in1: *const u32);
	fn ecclib_lookup_field_by_name(name: *const c_char) -> *const Field;
	fn ecclib_field_list() -> *const *const c_char;
	fn ecclib_field_name(field: *const Field) -> *const c_char;
	fn ecclib_field_element_size(field: *const Field) -> size_t;
	fn ecclib_field_field_bits(field: *const Field) -> size_t;
	fn ecclib_field_storage_octets(field: *const Field) -> size_t;
	fn ecclib_field_field_element_count(field: *const Field) -> *const u8;
	fn ecclib_field_field_characteristic(field: *const Field) -> *const u8;
	fn ecclib_field_degree(field: *const Field) -> size_t;
	fn ecclib_field_degree_stride(field: *const Field) -> size_t;
	fn ecclib_field_degree_bits(field: *const Field) -> size_t;
	fn ecclib_field_zero(field: *const Field, out: *mut c_void);
	fn ecclib_field_set_int(field: *const Field, out: *mut c_void, val: u32);
	fn ecclib_field_set(field: *const Field, out: *mut c_void, val: *const u8);
	fn ecclib_field_get(field: *const Field, in1: *const c_void, val: *mut u8);
	fn ecclib_field_check(field: *const Field, val: *const u8) -> c_int;
	fn ecclib_field_add(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_field_sub(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_field_mul(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_field_smul(field: *const Field, out: *mut c_void, in1: *const c_void, in2: u32);
	fn ecclib_field_neg(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_sqr(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_inv(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_sqrt(field: *const Field, out: *mut c_void, in1: *const c_void) -> c_int;
	fn ecclib_field_copy(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_load_cond(field: *const Field, out: *mut c_void, in1: *const c_void, flag: u32);
	fn ecclib_field_select(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void,
		flag: u32);
	fn ecclib_field_cswap(field: *const Field, x1: *mut c_void, x2: *mut c_void, flag: u32);
	fn ecclib_lookup_curve_by_name(name: *const c_char) -> *const Curve;
	fn ecclib_curve_list() -> *const *const c_char;
	fn ecclib_curve_name(curve: *const Curve) -> *const c_char;
	fn ecclib_curve_field(curve: *const Curve) -> *const Field;
	fn ecclib_curve_storage_octets(curve: *const Curve) -> size_t;
	fn ecclib_curve_element_size(curve: *const Curve) -> size_t;
	fn ecclib_curve_order(curve: *const Curve) -> *const u8;
	fn ecclib_curve_clog2l(curve: *const Curve) -> size_t;
	fn ecclib_curve_rlog2l(curve: *const Curve) -> size_t;
	fn ecclib_curve_partial_max_bits(curve: *const Curve) -> size_t;
	fn ecclib_curve_scalar_elems_single(curve: *const Curve) -> size_t;
	fn ecclib_curve_scalar_elems_double(curve: *const Curve) -> size_t;
	fn ecclib_curve_scalar_octets(curve: *const Curve) -> size_t;
	fn ecclib_curve_cofactor(curve: *const Curve) -> u32;
	fn ecclib_curve_flags(curve: *const Curve) -> u32;
	fn ecclib_curve_montgomery_base(curve: *const Curve) -> *const u8;
	fn ecclib_curve_reduce_partial(curve: *const Curve, output: *mut u32, input: *const u32);
	fn ecclib_curve_reduce_full(curve: *const Curve, output: *mut u8, input: *const u32);
	fn ecclib_curve_point_zero(curve: *const Curve, point: *mut c_void);
	fn ecclib_curve_point_base(curve: *const Curve, point: *mut c_void);
	fn ecclib_curve_point_set(curve: *const Curve, point: *mut c_void, data: *const u8) -> c_int;
	fn ecclib_curve_point_get(curve: *const Curve, point: *const c_void, output: *mut u8);
	fn ecclib_curve_point_add(curve: *const Curve, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_curve_point_double(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_mul_base(curve: *const Curve, out: *mut c_void, mul: *const u32);
	fn ecclib_curve_point_mul(curve: *const Curve, out: *mut c_void, mul: *const u32, in1: *const c_void);
	fn ecclib_curve_point_neg(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_copy(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_get_coords(curve: *const Curve, point: *const c_void, x: *mut c_void, y: *mut c_void);
	fn ecclib_curve_point_set_coords(curve: *const Curve, point: *mut c_void, x: *const c_void, y: *const c_void)
		-> c_int;
	fn ecclib_curve_point_montgomery(curve: *const Curve, output: *mut c_void, point: *const c_void) -> c_int;
	fn ecclib_curve_montgomery_mul(curve: *const Curve, output: *mut c_void, mul: *const u32,
		base: *const c_void) -> c_int;
	fn ecclib_xcurve_base(curve: *const Curve, pubkey: *mut u8, secret: *const u8) -> c_int;
	fn ecclib_xcurve(curve: *const Curve, shared: *mut u8, secret: *const u8, ppubkey: *const u8) -> c_int;
	fn ecclib_set_thread_fns(lock: extern fn(ctx: *mut c_void), unlock: extern fn(ctx: *mut c_void),
		ctx: *mut c_void);
	fn ecclib_zeroize(buf: *mut u8, buflen: size_t);
}

unsafe fn c_string_to_rust_string(string: *const c_char) -> &'static str
{
	//Count length.
	let mut len = 0isize;
	while *string.offset(len) != 0 { len += 1; }
	from_utf8_unchecked(from_raw_parts(string as *const u8, len as usize))
}

fn rust_string_to_c_string(_name: &mut [c_char], name: &str) -> Result<(), ()>
{
	//If name is too long, assume invalid.
	let bname = name.as_bytes();
	if bname.len() >= _name.len() { return Err(()); }
	for i in 0..bname.len() {
		if bname[i] == 0  { return Err(()); }	//Embedded NUL not allowed.
		_name[i] = bname[i] as c_char;
	}
	_name[bname.len()] = 0;
	Ok(())
}

impl Field
{
	///List all available hash function names.
	pub fn list() -> &'static [&'static str]
	{
		static INIT: Once = ONCE_INIT;
		INIT.call_once(|| { unsafe {
			let rlist = ecclib_field_list();
			//Intern all strings within.
			let mut idx = 0isize;
			while !(*rlist.offset(idx)).is_null() && (idx as usize) < MAX_FIELDS {
				FIELD_NAME_LIST[idx as usize] = c_string_to_rust_string(*rlist.offset(idx));
				idx += 1;
			}
			FIELD_NAME_LIST_LEN = idx as usize;
		}});
		unsafe{&FIELD_NAME_LIST[..FIELD_NAME_LIST_LEN]}
	}
	///Return object representing the hash function `name`.
	///
	///This method fails if the hash named is not supported.
	pub fn lookup(name: &str) -> Result<&'static Field, ()>
	{
		let mut _name = [0 as c_char; MAX_FIELD_NAME];
		try!(rust_string_to_c_string(&mut _name[..], name));
		unsafe {
			let rscheme = ecclib_lookup_field_by_name(_name.as_ptr());
			if !rscheme.is_null() {
				Ok(&*rscheme)
			} else {
				Err(())
			}	//Not found?
		}
	}
	///Get name of the field.
	pub fn name(&self) -> &'static str
	{
		unsafe{c_string_to_rust_string(ecclib_field_name(self._low()))}
	}
	///Get number of bits in field.
	pub fn field_bits(&self) -> usize
	{
		unsafe{ecclib_field_field_bits(self._low()) as usize}
	}
	///Get the storage size of field element in octets.
	pub fn storage_octets(&self) -> usize
	{
		unsafe{ecclib_field_storage_octets(self._low()) as usize}
	}
	///Get the number of elements in the field.
	pub fn field_element_count(&self) -> &'static [u8]
	{
		unsafe {
			let size = self.storage_octets();
			let mem = ecclib_field_field_element_count(self._low());
			from_raw_parts(mem, size)
		}
	}
	///Get the characteristic of the field.
	pub fn field_characteristic(&self) -> &'static [u8]
	{
		unsafe {
			let size = self.storage_octets();
			let mem = ecclib_field_field_characteristic(self._low());
			from_raw_parts(mem, size)
		}
	}
	///Get the degree of the field.
	pub fn degree(&self) -> usize
	{
		unsafe{ecclib_field_degree(self._low()) as usize}
	}
	///Get stride between the degrees of the field element.
	pub fn degree_stride(&self) -> usize
	{
		unsafe{ecclib_field_degree_stride(self._low()) as usize}
	}
	///Get number of bits in each degree.
	pub fn degree_bits(&self) -> usize
	{
		unsafe{ecclib_field_degree_bits(self._low()) as usize}
	}
	///Check if field element is in range.
	///
	///Fails if val is wrong number of octets or out of range.
	pub fn check(&self, val: &[u8]) -> Result<(), ()>
	{
		if val.len() != self.storage_octets() { return Err(()); }
		if unsafe{ecclib_field_check(self._low(), val.as_ptr() as *const u8)} > 0 { Ok(()) } else { Err(()) }
	}
	///Make a zero element of this field.
	pub fn make_zero(&'static self) -> FieldElement
	{
		let mut ret = self.create_uninit();
		ret.zero();
		ret
	}
	///Make an element with given small value (in zeroth degree) of this field.
	pub fn make_int(&'static self, val: u32) -> FieldElement
	{
		let mut ret = self.create_uninit();
		ret.set_int(val);
		ret
	}
	///Make an element with given serialized value.
	//
	//Fails if val is wrong number of octets.
	pub fn make(&'static self, val: &[u8]) -> Result<FieldElement, ()>
	{
		let mut ret = self.create_uninit();
		try!(ret.set(val));
		Ok(ret)
	}
	fn create_uninit(&'static self) -> FieldElement
	{
		FieldElement{field:self, storage:repeat(0u8).take(self.element_size()).collect()}
	}
	fn element_size(&self) -> usize
	{
		unsafe{ecclib_field_element_size(self._low()) as usize}
	}
	fn _low(&self) -> *const Field
	{
		self as *const Field
	}
}

impl Curve
{
	///List all available hash function names.
	pub fn list() -> &'static [&'static str]
	{
		static INIT: Once = ONCE_INIT;
		INIT.call_once(|| { unsafe {
			let rlist = ecclib_curve_list();
			//Intern all strings within.
			let mut idx = 0isize;
			while !(*rlist.offset(idx)).is_null() && (idx as usize) < MAX_CURVES {
				CURVE_NAME_LIST[idx as usize] = c_string_to_rust_string(*rlist.offset(idx));
				idx += 1;
			}
			CURVE_NAME_LIST_LEN = idx as usize;
		}});
		unsafe{&CURVE_NAME_LIST[..CURVE_NAME_LIST_LEN]}
	}
	///Return object representing the curve `name`.
	///
	///This method fails if the curve named is not supported.
	pub fn lookup(name: &str) -> Result<&'static Curve, ()>
	{
		let mut _name = [0 as c_char; MAX_CURVE_NAME];
		try!(rust_string_to_c_string(&mut _name[..], name));
		unsafe {
			let rscheme = ecclib_lookup_curve_by_name(_name.as_ptr());
			if !rscheme.is_null() {
				Ok(&*rscheme)
			} else {
				Err(())
			}	//Not found?
		}
	}
	///Get name of the curve.
	pub fn name(&self) -> &'static str
	{
		unsafe{c_string_to_rust_string(ecclib_curve_name(self._low()))}
	}
	///Get the field underlying the curve.
	pub fn field(&self) -> &'static Field
	{
		unsafe {
			let raw = ecclib_curve_field(self._low());
			&*raw
		}
	}
	///Get number of octets storing curve point takes.
	pub fn storage_octets(&self) -> usize
	{
		unsafe{ecclib_curve_storage_octets(self._low()) as usize}
	}
	///Get the order of the curve as little-endian integer.
	pub fn order(&self) -> &'static [u8]
	{
		unsafe {
			let size = self.storage_octets();
			let mem = ecclib_curve_order(self._low());
			from_raw_parts(mem, size)
		}
	}
	///Get ceil(log2(x)) of the order of the curve.
	pub fn clog2l(&self) -> usize
	{
		unsafe{ecclib_curve_clog2l(self._low()) as usize}
	}
	///Get round(log2(x)) of the order of the curve.
	pub fn rlog2l(&self) -> usize
	{
		unsafe{ecclib_curve_rlog2l(self._low()) as usize}
	}
	///Get The maximum number of bits in scalar that reduce_partial can reduce.
	pub fn partial_max_bits(&self) -> usize
	{
		unsafe{ecclib_curve_partial_max_bits(self._low()) as usize}
	}
	///Get number of elements in single-width scalar.
	pub fn scalar_elems_single(&self) -> usize
	{
		unsafe{ecclib_curve_scalar_elems_single(self._low()) as usize}
	}
	///Get number of elements in double-width scalar (always 2 times the number in single-width).
	pub fn scalar_elems_double(&self) -> usize
	{
		unsafe{ecclib_curve_scalar_elems_double(self._low()) as usize}
	}
	///Get number of octets serializing a scalar takes.
	pub fn scalar_octets(&self) -> usize
	{
		unsafe{ecclib_curve_scalar_octets(self._low()) as usize}
	}
	///Get the cofactor of the curve.
	pub fn cofactor(&self) -> u32
	{
		unsafe{ecclib_curve_cofactor(self._low())}
	}
	///Get the flags for the curve.
	pub fn flags(&self) -> u32
	{
		unsafe{ecclib_curve_flags(self._low())}
	}
	///Get the serialization of standard montgomery basepoint.
	pub fn montgomery_base(&self) -> &'static [u8]
	{
		unsafe {
			let mem = ecclib_curve_montgomery_base(self._low());
			let size = self.field().storage_octets();
			from_raw_parts(mem, size)
		}
	}
	///Partially reduce a scalar.
	///
	///This reduces a scalar from double width to single width (possibly not fully).
	///
	///The reduced value is small enough to add with single width, followed by multiply to double width.
	///
	///Fails if source or target has the wrong number of elements.
	pub fn reduce_partial(&self, output: &mut [u32], input: &[u32]) -> Result<(), ()>
	{
		if output.len() != self.scalar_elems_single() { return Err(()); }
		if input.len() != self.scalar_elems_double() { return Err(()); }
		unsafe{ecclib_curve_reduce_partial(self._low(), output.as_mut_ptr() as *mut u32,
			input.as_ptr() as *const u32)};
		Ok(())
	}
	///Fully reduce and serialize a scalar.
	///
	///This fully reduces a single-width scalar and then serializes it.
	///
	///Fails if source is not single-width scalar or if target is not scalar storage octets elements.
	pub fn reduce_full(&self, output: &mut [u8], input: &[u32]) -> Result<(), ()>
	{
		if output.len() != self.scalar_octets() { return Err(()); }
		if input.len() != self.scalar_elems_single() { return Err(()); }
		unsafe{ecclib_curve_reduce_full(self._low(), output.as_mut_ptr() as *mut u8,
			input.as_ptr() as *const u32)};
		Ok(())
	}
	///Create a zero point.
	pub fn make_zero(&'static self) -> Point
	{
		let mut ret = self.create_uninit();
		ret.zero();
		ret
	}
	///Create a curve standard basepoint.
	pub fn make_base(&'static self) -> Point
	{
		let mut ret = self.create_uninit();
		ret.base();
		ret
	}
	///Create a curve point with specified serialized value.
	///
	///Fails if the point is invalid, or wrong number of octets.
	pub fn make(&'static self, val: &[u8]) -> Result<Point, ()>
	{
		let mut ret = self.create_uninit();
		try!(ret.set(val));
		Ok(ret)
	}
	///Perform Montgomery multiplication.
	///
	///Fails if mul is not single-width scalar or if the curve does not support montgomery form.
	pub fn montgomery_mul(&self, output: &mut FieldElement, mul: &[u32], base: &FieldElement) -> Result<(), ()>
	{
		let field = self.field();
		if field != output.field || field != base.field || self.scalar_elems_single() != mul.len() {
			return Err(());
		}
		if unsafe{ecclib_curve_montgomery_mul(self._low(), output._lowm(), mul.as_ptr() as *const u32, 
			base._low())} <= 0 {
			return Err(());
		}
		Ok(())
	}
	///Perform X* DHF with base point.
	///
	///Fails if pubkey or secret is not field serialization sized.
	pub fn xcurve_base(&self, pubkey: &mut [u8], secret: &[u8]) -> Result<(), ()>
	{
		let size = self.field().storage_octets();
		if pubkey.len() != size || secret.len() != size { return Err(()); }
		if unsafe{ecclib_xcurve_base(self._low(), pubkey.as_mut_ptr() as *mut u8,
			secret.as_ptr() as *const u8)} <= 0 {
			return Err(());
		}
		Ok(())
		
	}
	///Perform X* DHF with peer key.
	///
	///Fails if shared, secret or ppubkey is not field serialization sized, or if result of operation is all
	///zeroes (bad peer key).
	pub fn xcurve(&self, shared: &mut [u8], secret: &[u8], ppubkey: &[u8]) -> Result<(), ()>
	{
		let size = self.field().storage_octets();
		if ppubkey.len() != size || secret.len() != size || shared.len() != size { return Err(()); }
		if unsafe{ecclib_xcurve(self._low(), shared.as_mut_ptr() as *mut u8,
			secret.as_ptr() as *const u8, ppubkey.as_ptr() as *const u8)} <= 0 {
			return Err(());
		}
		let mut syndrome = 0u8;
		for i in 0..shared.len() { syndrome |= shared[i]; }
		if syndrome == 0 { return Err(()); }
		Ok(())
	}
	fn create_uninit(&'static self) -> Point
	{
		Point{curve:self, storage:repeat(0u8).take(self.element_size()).collect()}
	}
	fn element_size(&self) -> usize
	{
		unsafe{ecclib_curve_element_size(self._low()) as usize}
	}
	fn _low(&self) -> *const Curve
	{
		self as *const Curve
	}
}

impl FieldElement
{
	pub fn zero(&mut self)
	{
		unsafe{ecclib_field_zero(self.field._low(), self._lowm())};
	}
	pub fn set_int(&mut self, val: u32)
	{
		unsafe{ecclib_field_set_int(self.field._low(), self._lowm(), val)};
	}
	pub fn set(&mut self, val: &[u8]) -> Result<(), ()>
	{
		if val.len() != self.field.storage_octets() { return Err(()); }
		unsafe{ecclib_field_set(self.field._low(), self._lowm(), val.as_ptr() as *const u8)};
		Ok(())
	}
	pub fn get(&self, val: &mut [u8]) -> Result<(), ()>
	{
		if val.len() != self.field.storage_octets() { return Err(()); }
		unsafe{ecclib_field_get(self.field._low(), self._low(), val.as_mut_ptr() as *mut u8)};
		Ok(())
	}
	pub fn add(&mut self, in1: &FieldElement, in2: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field || self.field != in2.field { return Err(()); }
		unsafe{ecclib_field_add(self.field._low(), self._lowm(), in1._low(), in2._low())};
		Ok(())
	}
	pub fn sub(&mut self, in1: &FieldElement, in2: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field || self.field != in2.field { return Err(()); }
		unsafe{ecclib_field_sub(self.field._low(), self._lowm(), in1._low(), in2._low())};
		Ok(())
	}
	pub fn mul(&mut self, in1: &FieldElement, in2: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field || self.field != in2.field { return Err(()); }
		unsafe{ecclib_field_mul(self.field._low(), self._lowm(), in1._low(), in2._low())};
		Ok(())
	}
	pub fn smul(&mut self, in1: &FieldElement, in2: u32) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		unsafe{ecclib_field_smul(self.field._low(), self._lowm(), in1._low(), in2)};
		Ok(())
	}
	pub fn neg(&mut self, in1: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		unsafe{ecclib_field_neg(self.field._low(), self._lowm(), in1._low())};
		Ok(())
	}
	pub fn sqr(&mut self, in1: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		unsafe{ecclib_field_sqr(self.field._low(), self._lowm(), in1._low())};
		Ok(())
	}
	pub fn inv(&mut self, in1: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		unsafe{ecclib_field_inv(self.field._low(), self._lowm(), in1._low())};
		Ok(())
	}
	pub fn sqrt(&mut self, in1: &FieldElement) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		if unsafe{ecclib_field_sqrt(self.field._low(), self._lowm(), in1._low())} <= 0 { return Err(()); }
		Ok(())
	}
	pub fn load_cond(&mut self, in1: &FieldElement, flag: u32) -> Result<(), ()>
	{
		if self.field != in1.field { return Err(()); }
		unsafe{ecclib_field_load_cond(self.field._low(), self._lowm(), in1._low(), flag)};
		Ok(())
	}
	pub fn select(&mut self, in1: &FieldElement, in2: &FieldElement, flag: u32) -> Result<(), ()>
	{
		if self.field != in1.field || self.field != in2.field { return Err(()); }
		unsafe{ecclib_field_select(self.field._low(), self._lowm(), in1._low(), in2._low(), flag)};
		Ok(())
	}
	pub fn cswap(&mut self, x2: &mut FieldElement, flag: u32) -> Result<(), ()>
	{
		if self.field != x2.field { return Err(()); }
		unsafe{ecclib_field_cswap(self.field._low(), self._lowm(), x2._lowm(), flag)};
		Ok(())
	}
	fn _low(&self) -> *const c_void
	{
		self.storage.as_ptr() as *const c_void
	}
	fn _lowm(&mut self) -> *mut c_void
	{
		self.storage.as_mut_ptr() as *mut c_void
	}
}

impl Point
{
	pub fn zero(&mut self)
	{
		unsafe{ecclib_curve_point_zero(self.curve._low(), self._lowm())};
	}
	pub fn base(&mut self)
	{
		unsafe{ecclib_curve_point_base(self.curve._low(), self._lowm())};
	}
	pub fn set(&mut self, data: &[u8]) -> Result<(), ()>
	{
		if data.len() != self.curve.storage_octets() { return Err(()); }
		if unsafe{ecclib_curve_point_set(self.curve._low(), self._lowm(), data.as_ptr() as *const u8)} <= 0 {
			return Err(());
		}
		Ok(())
	}
	pub fn get(&self, output: &mut [u8]) -> Result<(), ()>
	{
		if output.len() != self.curve.storage_octets() { return Err(()); }
		unsafe{ecclib_curve_point_get(self.curve._low(), self._low(), output.as_mut_ptr() as *mut u8)};
		Ok(())
	}
	pub fn add(&mut self, in1: &Point, in2: &Point) -> Result<(), ()>
	{
		if self.curve != in1.curve || self.curve != in2.curve { return Err(()); }
		unsafe{ecclib_curve_point_add(self.curve._low(), self._lowm(), in1._low(), in2._low())};
		Ok(())
	}
	pub fn double(&mut self, in1: &Point) -> Result<(), ()>
	{
		if self.curve != in1.curve { return Err(()); }
		unsafe{ecclib_curve_point_double(self.curve._low(), self._lowm(), in1._low())};
		Ok(())
	}
	pub fn mul_base(&mut self, mul: &[u32]) -> Result<(), ()>
	{
		if self.curve.scalar_elems_single() != mul.len() { return Err(()); }
		unsafe{ecclib_curve_point_mul_base(self.curve._low(), self._lowm(), mul.as_ptr() as *const u32)};
		Ok(())
	}
	pub fn mul(&mut self, mul: &[u32], in1: &Point) -> Result<(), ()>
	{
		if self.curve != in1.curve || self.curve.scalar_elems_single() != mul.len() { return Err(()); }
		unsafe{ecclib_curve_point_mul(self.curve._low(), self._lowm(), mul.as_ptr() as *const u32,
			in1._low())};
		Ok(())
	}
	pub fn neg(&mut self, in1: &Point) -> Result<(), ()>
	{
		if self.curve != in1.curve { return Err(()); }
		unsafe{ecclib_curve_point_neg(self.curve._low(), self._lowm(), in1._low())};
		Ok(())
	}
	pub fn get_coords(&self, x: &mut FieldElement, y: &mut FieldElement) -> Result<(), ()>
	{
		let field = self.curve.field();
		if field != x.field || field != y.field { return Err(()); }
		unsafe{ecclib_curve_point_get_coords(self.curve._low(), self._low(), x._lowm(), y._lowm())};
		Ok(())
	}
	pub fn set_coords(&mut self, x: &FieldElement, y: &FieldElement) -> Result<(), ()>
	{
		let field = self.curve.field();
		if field != x.field || field != y.field { return Err(()); }
		if unsafe{ecclib_curve_point_set_coords(self.curve._low(), self._lowm(), x._low(), y._low())} <= 0 {
			return Err(());
		}
		Ok(())
	}
	pub fn montgomery(&self, output: &mut FieldElement) -> Result<(), ()>
	{
		if self.curve.field() != output.field { return Err(()); }
		if unsafe{ecclib_curve_point_montgomery(self.curve._low(), output._lowm(), self._low())} <= 0 {
			return Err(());
		}
		Ok(())
		
	}
	fn _low(&self) -> *const c_void
	{
		self.storage.as_ptr() as *const c_void
	}
	fn _lowm(&mut self) -> *mut c_void
	{
		self.storage.as_mut_ptr() as *mut c_void
	}
}

pub fn zeroize(buf: &mut [u8])
{
	unsafe{ecclib_zeroize(buf.as_mut_ptr() as *mut u8, buf.len())};
}

pub fn bigint_add(out: &mut [u32], in1: &[u32], in2: &[u32]) -> Result<(), ()>
{
	if out.len() != in1.len() || in1.len() != in2.len() { return Err(()); }
	unsafe{ecclib_bigint_add(out.as_mut_ptr() as *mut u32, in1.as_ptr() as *const u32,
		in2.as_ptr() as *const u32, in1.len())};
	Ok(())
}

pub fn bigint_sub(out: &mut [u32], in1: &[u32], in2: &[u32]) -> Result<(), ()>
{
	if out.len() != in1.len() || in1.len() != in2.len() { return Err(()); }
	unsafe{ecclib_bigint_sub(out.as_mut_ptr() as *mut u32, in1.as_ptr() as *const u32,
		in2.as_ptr() as *const u32, in1.len() as size_t)};
	Ok(())
}

pub fn bigint_mul(out: &mut [u32], in1: &[u32], in2: &[u32]) -> Result<(), ()>
{
	if out.len() != in1.len() + in2.len() { return Err(()); }
	unsafe{ecclib_bigint_mul(out.as_mut_ptr() as *mut u32, in1.as_ptr() as *const u32, in1.len() as size_t,
		in2.as_ptr() as *const u32, in2.len() as size_t)};
	Ok(())
}

pub fn bigint_load(out: &mut [u32], in1: &[u8])
{
	unsafe{ecclib_bigint_load(out.as_mut_ptr() as *mut u32, out.len(), in1.as_ptr() as *const u8,
		in1.len() as size_t)};
}

pub fn bigint_store(out: &mut [u8], in1: &[u32]) -> Result<(), ()>
{
	if in1.len() * 4 < out.len() { return Err(()); }
	unsafe{ecclib_bigint_store(out.as_mut_ptr() as *mut u8, out.len(), in1.as_ptr() as *const u32)};
	Ok(())
}

struct WaitStruct
{
	lock: Mutex<bool>,
	condition: Condvar
}

extern fn threadctx_lock(ctx: *mut c_void)
{
	let ws = unsafe{&*(ctx as *mut WaitStruct)};
	loop {
		let mut guard = ws.lock.lock().unwrap();
		if *guard == false {
			*guard = true;
			return;
		}
		let _ = ws.condition.wait(guard).unwrap();
	}
}

extern fn threadctx_unlock(ctx: *mut c_void)
{
	let ws = unsafe{&*(ctx as *mut WaitStruct)};
	let mut guard = ws.lock.lock().unwrap();
	*guard = false;
	ws.condition.notify_one();
}

fn threadctx_init() -> *mut c_void
{
	let ws = Box::new(WaitStruct{lock:Mutex::new(false), condition:Condvar::new()});
	Box::into_raw(ws) as *mut c_void
}

pub fn call_set_thread_fns()
{
	static INIT: Once = ONCE_INIT;
	INIT.call_once(|| { unsafe {
		ecclib_set_thread_fns(threadctx_lock, threadctx_unlock, threadctx_init());
	}});
}

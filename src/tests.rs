use super::{Field,Curve,FLAG_HAS_MONTGOMERY,FLAG_MONTGOMERY_SLOW,bigint_add,bigint_sub,bigint_mul};

#[test]
fn test_bigint_add_size_mismatch()
{
	let mut a = [0u32; 4];
	let mut b = [0u32; 4];
	let mut c = [0u32; 5];
	bigint_add(&mut a[..], &b[..], &c[..]).unwrap_err();
	bigint_add(&mut b[..], &c[..], &a[..]).unwrap_err();
	bigint_add(&mut c[..], &a[..], &b[..]).unwrap_err();
}

#[test]
fn test_bigint_add()
{
	let mut a = [0u32; 3];
	let b = [0xc0593dd5u32, 0x7e215ccbu32, 0x031ec37bu32];
	let c = [0x23b674cbu32, 0xec90fb0fu32, 0x2af5adf4u32];
	bigint_add(&mut a[..], &b[..], &c[..]).unwrap();
	assert_eq!(&a[..], &[0xe40fb2a0u32, 0x6ab257dau32, 0x2e147170u32]);
}

#[test]
fn test_bigint_sub_size_mismatch()
{
	let mut a = [0u32; 4];
	let mut b = [0u32; 4];
	let mut c = [0u32; 5];
	bigint_sub(&mut a[..], &b[..], &c[..]).unwrap_err();
	bigint_sub(&mut b[..], &c[..], &a[..]).unwrap_err();
	bigint_sub(&mut c[..], &a[..], &b[..]).unwrap_err();
}

#[test]
fn test_bigint_sub()
{
	let mut a = [0u32; 3];
	let b = [0x23b674cbu32, 0xec90fb0fu32, 0x2af5adf4u32];
	let c = [0xc0593dd5u32, 0x7e215ccbu32, 0x031ec37bu32];
	bigint_sub(&mut a[..], &b[..], &c[..]).unwrap();
	assert_eq!(&a[..], &[0x635d36f6u32, 0x6e6f9e43u32, 0x27d6ea79u32]);
}    

#[test]
fn test_bigint_mul_size_mismatch()
{
	let mut aa = [0u32; 8];
	let mut ab = [0u32; 10];
	let b = [0u32; 4];
	let c = [0u32; 5];
	bigint_mul(&mut aa[..], &b[..], &c[..]).unwrap_err();
	bigint_mul(&mut ab[..], &b[..], &c[..]).unwrap_err();
}

#[test]
fn test_bigint_mul()
{
	let mut a = [0u32; 6];
	let b = [0x23b674cbu32, 0xec90fb0fu32, 0x2af5adf4u32];
	let c = [0xc0593dd5u32, 0x7e215ccbu32, 0x031ec37bu32];
	bigint_mul(&mut a[..], &b[..], &c[..]).unwrap();
	assert_eq!(&a[..], &[
		0x0b368be7u32, 0xdf52521bu32, 0x2a92c03au32,
		0x26b487f9u32, 0x1e3015abu32, 0x00860aa2u32
	]);
}    




 
#[test]
fn print_avail_fields()
{
	let list = Field::list();
	assert_eq!(list.iter().count(), 6);
	for i in list.iter() {
		Field::lookup(i).unwrap();
	}
}

#[test]
fn lookup_nonexistent_field()
{
	Field::lookup("DOES-NOT-EXIST").unwrap_err();
}

#[test]
fn lookup_field_test()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().name(), "2^255-19");
}

#[test]
fn field_storage_octets_test()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().storage_octets(), 32);
	assert_eq!(Field::lookup("2^448-2^224-1").unwrap().storage_octets(), 56);
}

#[test]
fn field_bits_test()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().field_bits(), 255);
	assert_eq!(Field::lookup("2^448-2^224-1").unwrap().field_bits(), 448);
}

#[test]
fn field_degree()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().degree(), 1);
	assert_eq!(Field::lookup("2^448-2^224-1").unwrap().degree(), 1);
}

#[test]
fn field_degree_stride()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().degree_stride(), 32);
	assert_eq!(Field::lookup("2^448-2^224-1").unwrap().degree_stride(), 56);
}

#[test]
fn field_degree_bits()
{
	assert_eq!(Field::lookup("2^255-19").unwrap().degree_bits(), 255);
	assert_eq!(Field::lookup("2^448-2^224-1").unwrap().degree_bits(), 448);
}

#[test]
fn field_characteristic()
{
	let f = Field::lookup("2^255-19").unwrap();
	assert_eq!(f.field_characteristic(), f.field_element_count());
	let f = Field::lookup("2^448-2^224-1").unwrap();
	assert_eq!(f.field_characteristic(), f.field_element_count());
}

#[test]
fn field_element_count()
{
	let f = Field::lookup("2^255-19").unwrap();
	let fe = f.field_element_count();
	for i in 0..fe.len() {
		let expect = if i == 0 { 0xED } else if i == 31 { 0x7F } else { 0xFF };
		assert_eq!(fe[i], expect);
	}
	let f = Field::lookup("2^448-2^224-1").unwrap();
	let fe = f.field_element_count();
	for i in 0..fe.len() {
		let expect = if i == 28 { 0xFE } else { 0xFF };
		assert_eq!(fe[i], expect);
	}
}

#[test]
fn field_add_simple()
{
	let list = Field::list();
	for i in list.iter() {
		let mut buf1 = [1u8; 100];
		let mut buf2 = [1u8; 100];
		let f = Field::lookup(i).unwrap();
		let fe1 = f.make_int(123);
		let fe2 = f.make_int(543);
		let mut fe4 = f.make_zero();
		let fe3 = f.make_int(666);
		fe4.add(&fe1, &fe2).unwrap();
		fe3.get(&mut buf1[..f.storage_octets()]).unwrap();
		fe4.get(&mut buf2[..f.storage_octets()]).unwrap();
		assert_eq!(&buf1[..], &buf2[..]);
	}
}

#[test]
fn field_store_zero_background()
{
	let list = Field::list();
	for i in list.iter() {
		let f = Field::lookup(i).unwrap();
		let mut buf1 = [0u8; 100];
		let mut buf2 = [255u8; 100];
		let z = f.make_zero();
		z.get(&mut buf1[..f.storage_octets()]).unwrap();
		z.get(&mut buf2[..f.storage_octets()]).unwrap();
		assert_eq!(&buf1[..f.storage_octets()], &buf2[..f.storage_octets()]);
	}
}

#[test]
fn field_add_sub_neg_consistent()
{
	let list = Field::list();
	for i in list.iter() {
		let f = Field::lookup(i).unwrap();
		let mut buf1 = [0u8; 100];
		let mut buf2 = [0u8; 100];
		let buf3 = [0u8; 100];
		let mut s1 = [0u8; 100];
		let mut s2 = [0u8; 100];
		for i in 0..s1.len() { s1[i] = 127u8.wrapping_mul(i as u8).wrapping_add(43); }
		for i in 0..s2.len() { s2[i] = 59u8.wrapping_mul(i as u8).wrapping_add(101); }
		let a = f.make(&s1[..f.storage_octets()]).unwrap();
		let b = f.make(&s2[..f.storage_octets()]).unwrap();
		let mut na = f.make_zero();
		let mut nb = f.make_zero();
		let mut apb = f.make_zero();
		let mut apnb = f.make_zero();
		let mut amb = f.make_zero();
		let mut apbmb = f.make_zero();
		let mut apna = f.make_zero();
		na.neg(&a).unwrap();
		nb.neg(&b).unwrap();
		apb.add(&a, &b).unwrap();
		apnb.add(&a, &nb).unwrap();
		amb.sub(&a, &b).unwrap();
		apbmb.sub(&apb, &b).unwrap();
		apna.add(&a, &na).unwrap();
		apbmb.get(&mut buf1[..f.storage_octets()]).unwrap();
		a.get(&mut buf2[..f.storage_octets()]).unwrap();
		assert_eq!(&buf1[..], &buf2[..]);
		amb.get(&mut buf1[..f.storage_octets()]).unwrap();
		apnb.get(&mut buf2[..f.storage_octets()]).unwrap();
		assert_eq!(&buf1[..], &buf2[..]);
		apna.get(&mut buf1[..f.storage_octets()]).unwrap();
		assert_eq!(&buf1[..], &buf3[..]);
	}
}

#[test]
fn print_avail_curves()
{
	let list = Curve::list();
	assert_eq!(list.iter().count(), 8);
	for i in list.iter() {
		Curve::lookup(i).unwrap();
	}
}

#[test]
fn lookup_nonexistent_curve()
{
	Curve::lookup("DOES-NOT-EXIST").unwrap_err();
}

#[test]
fn lookup_curve_test()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().name(), "curve25519");
}

#[test]
fn curve_field_test()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().field().name(), "2^255-19");
	assert_eq!(Curve::lookup("curve448").unwrap().field().name(), "2^448-2^224-1");
}

#[test]
fn curve_storage_octets_test()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().storage_octets(), 32);
	assert_eq!(Curve::lookup("curve448").unwrap().storage_octets(), 57);
	assert_eq!(Curve::lookup("E-521").unwrap().storage_octets(), 66);
}

#[test]
fn curve_log2l()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().clog2l(), 253);
	assert_eq!(Curve::lookup("curve25519").unwrap().rlog2l(), 252);
	assert_eq!(Curve::lookup("curve448").unwrap().clog2l(), 446);
	assert_eq!(Curve::lookup("curve448").unwrap().rlog2l(), 446);
}

#[test]
fn curve_scalar_octets()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().scalar_octets(), 32);
	assert_eq!(Curve::lookup("curve448").unwrap().scalar_octets(), 56);
	assert_eq!(Curve::lookup("E-521").unwrap().scalar_octets(), 65);
}

#[test]
fn curve_cofactor()
{
	assert_eq!(Curve::lookup("curve25519").unwrap().cofactor(), 8);
	assert_eq!(Curve::lookup("curve448").unwrap().cofactor(), 4);
	assert_eq!(Curve::lookup("E-521").unwrap().cofactor(), 4);
}

#[test]
fn curve_flags()
{
	let mask = FLAG_HAS_MONTGOMERY | FLAG_MONTGOMERY_SLOW;
	let rval = FLAG_HAS_MONTGOMERY;
	assert_eq!(Curve::lookup("curve25519").unwrap().flags() & mask, rval);
	assert_eq!(Curve::lookup("curve448").unwrap().flags() & mask, rval);
	assert_eq!(Curve::lookup("E-521").unwrap().flags() & mask, rval);
}

#[test]
fn curve_partial_max()
{
	let list = Curve::list();
	for i in list.iter() {
		let crv = Curve::lookup(i).unwrap();
		let pmax = crv.partial_max_bits();
		assert!(pmax >= (crv.clog2l()+1)*2 && pmax <= 32 * crv.scalar_elems_double());
	}
}

#[test]
fn curve_scalar_elements()
{
	let list = Curve::list();
	for i in list.iter() {
		let crv = Curve::lookup(i).unwrap();
		let sc = crv.scalar_elems_single();
		let dc = crv.scalar_elems_double();
		assert_eq!(dc, 2 * sc);
		assert!(sc * 32 >= crv.clog2l()+1);
	}
}

#[cfg(test)]
const STORAGE_MAX: usize = 66;

#[test]
fn curve_xcurve_consistent()
{
	let list = Curve::list();
	for i in list.iter() {
		let crv = Curve::lookup(i).unwrap();
		let ksize = crv.field().storage_octets();
		let b = crv.montgomery_base();
		let mut s1 = [0u8; STORAGE_MAX];
		let mut s2 = [0u8; STORAGE_MAX];
		let mut p1 = [0u8; STORAGE_MAX];
		let mut p2 = [0u8; STORAGE_MAX];
		let mut p1b = [0u8; STORAGE_MAX];
		let mut p2b = [0u8; STORAGE_MAX];
		let mut k1 = [0u8; STORAGE_MAX];
		let mut k2 = [0u8; STORAGE_MAX];
		for i in 0..s1.len() { s1[i] = 127u8.wrapping_mul(i as u8).wrapping_add(43); }
		for i in 0..s2.len() { s2[i] = 59u8.wrapping_mul(i as u8).wrapping_add(101); }
		crv.xcurve_base(&mut p1[..ksize], &s1[..ksize]).unwrap();
		crv.xcurve_base(&mut p2[..ksize], &s2[..ksize]).unwrap();
		crv.xcurve(&mut p1b[..ksize], &s1[..ksize], &b).unwrap();
		crv.xcurve(&mut p2b[..ksize], &s2[..ksize], &b).unwrap();
		crv.xcurve(&mut k1[..ksize], &s1[..ksize], &p2[..ksize]).unwrap();
		crv.xcurve(&mut k2[..ksize], &s2[..ksize], &p1[..ksize]).unwrap();
		assert_eq!(&p1[..], &p1b[..]);
		assert_eq!(&p2[..], &p2b[..]);
		assert_eq!(&k1[..], &k2[..]);
	}
}

#[test]
fn curve_x25519_rfc7748()
{
	let x25519 = Curve::lookup("curve25519").unwrap();
	let sa: [u8; 32] = [
		0x77, 0x07, 0x6d, 0x0a, 0x73, 0x18, 0xa5, 0x7d, 0x3c, 0x16, 0xc1, 0x72, 0x51, 0xb2, 0x66, 0x45,
		0xdf, 0x4c, 0x2f, 0x87, 0xeb, 0xc0, 0x99, 0x2a, 0xb1, 0x77, 0xfb, 0xa5, 0x1d, 0xb9, 0x2c, 0x2a
	];
	let sb: [u8; 32] = [
		0x5d, 0xab, 0x08, 0x7e, 0x62, 0x4a, 0x8a, 0x4b, 0x79, 0xe1, 0x7f, 0x8b, 0x83, 0x80, 0x0e, 0xe6,
		0x6f, 0x3b, 0xb1, 0x29, 0x26, 0x18, 0xb6, 0xfd, 0x1c, 0x2f, 0x8b, 0x27, 0xff, 0x88, 0xe0, 0xeb
	];
	let pac: [u8; 32] = [
		0x85, 0x20, 0xf0, 0x09, 0x89, 0x30, 0xa7, 0x54, 0x74, 0x8b, 0x7d, 0xdc, 0xb4, 0x3e, 0xf7, 0x5a,
		0x0d, 0xbf, 0x3a, 0x0d, 0x26, 0x38, 0x1a, 0xf4, 0xeb, 0xa4, 0xa9, 0x8e, 0xaa, 0x9b, 0x4e, 0x6a
	];
	let pbc: [u8; 32] = [
		0xde, 0x9e, 0xdb, 0x7d, 0x7b, 0x7d, 0xc1, 0xb4, 0xd3, 0x5b, 0x61, 0xc2, 0xec, 0xe4, 0x35, 0x37,
		0x3f, 0x83, 0x43, 0xc8, 0x5b, 0x78, 0x67, 0x4d, 0xad, 0xfc, 0x7e, 0x14, 0x6f, 0x88, 0x2b, 0x4f
	];
	let kc: [u8; 32] = [
		0x4a, 0x5d, 0x9d, 0x5b, 0xa4, 0xce, 0x2d, 0xe1, 0x72, 0x8e, 0x3b, 0xf4, 0x80, 0x35, 0x0f, 0x25,
		0xe0, 0x7e, 0x21, 0xc9, 0x47, 0xd1, 0x9e, 0x33, 0x76, 0xf0, 0x9b, 0x3c, 0x1e, 0x16, 0x17, 0x42
	];
	let mut pa = [0u8; 32];
	let mut pb = [0u8; 32];
	let mut ka = [0u8; 32];
	let mut kb = [0u8; 32];
	x25519.xcurve_base(&mut pa[..], &sa[..]).unwrap();
	x25519.xcurve_base(&mut pb[..], &sb[..]).unwrap();
	x25519.xcurve(&mut ka[..], &sa[..], &pb[..]).unwrap();
	x25519.xcurve(&mut kb[..], &sb[..], &pa[..]).unwrap();
	assert_eq!(&pa[..], &pac[..]);
	assert_eq!(&pb[..], &pbc[..]);
	assert_eq!(&ka[..], &kc[..]);
	assert_eq!(&kb[..], &kc[..]);
}

#[test]
fn curve_x448_rfc7748()
{
	let x448 = Curve::lookup("curve448").unwrap();
	let sa: [u8; 56] = [
		0x9a, 0x8f, 0x49, 0x25, 0xd1, 0x51, 0x9f, 0x57, 0x75, 0xcf, 0x46, 0xb0, 0x4b, 0x58, 0x00, 0xd4,
		0xee, 0x9e, 0xe8, 0xba, 0xe8, 0xbc, 0x55, 0x65, 0xd4, 0x98, 0xc2, 0x8d, 0xd9, 0xc9, 0xba, 0xf5,
		0x74, 0xa9, 0x41, 0x97, 0x44, 0x89, 0x73, 0x91, 0x00, 0x63, 0x82, 0xa6, 0xf1, 0x27, 0xab, 0x1d,
		0x9a, 0xc2, 0xd8, 0xc0, 0xa5, 0x98, 0x72, 0x6b
	];
	let sb: [u8; 56] = [
		0x1c, 0x30, 0x6a, 0x7a, 0xc2, 0xa0, 0xe2, 0xe0, 0x99, 0x0b, 0x29, 0x44, 0x70, 0xcb, 0xa3, 0x39,
		0xe6, 0x45, 0x37, 0x72, 0xb0, 0x75, 0x81, 0x1d, 0x8f, 0xad, 0x0d, 0x1d, 0x69, 0x27, 0xc1, 0x20,
		0xbb, 0x5e, 0xe8, 0x97, 0x2b, 0x0d, 0x3e, 0x21, 0x37, 0x4c, 0x9c, 0x92, 0x1b, 0x09, 0xd1, 0xb0,
		0x36, 0x6f, 0x10, 0xb6, 0x51, 0x73, 0x99, 0x2d
	];
	let pac: [u8; 56] = [
		0x9b, 0x08, 0xf7, 0xcc, 0x31, 0xb7, 0xe3, 0xe6, 0x7d, 0x22, 0xd5, 0xae, 0xa1, 0x21, 0x07, 0x4a,
		0x27, 0x3b, 0xd2, 0xb8, 0x3d, 0xe0, 0x9c, 0x63, 0xfa, 0xa7, 0x3d, 0x2c, 0x22, 0xc5, 0xd9, 0xbb,
		0xc8, 0x36, 0x64, 0x72, 0x41, 0xd9, 0x53, 0xd4, 0x0c, 0x5b, 0x12, 0xda, 0x88, 0x12, 0x0d, 0x53,
		0x17, 0x7f, 0x80, 0xe5, 0x32, 0xc4, 0x1f, 0xa0
	];
	let pbc: [u8; 56] = [
		0x3e, 0xb7, 0xa8, 0x29, 0xb0, 0xcd, 0x20, 0xf5, 0xbc, 0xfc, 0x0b, 0x59, 0x9b, 0x6f, 0xec, 0xcf,
		0x6d, 0xa4, 0x62, 0x71, 0x07, 0xbd, 0xb0, 0xd4, 0xf3, 0x45, 0xb4, 0x30, 0x27, 0xd8, 0xb9, 0x72,
		0xfc, 0x3e, 0x34, 0xfb, 0x42, 0x32, 0xa1, 0x3c, 0xa7, 0x06, 0xdc, 0xb5, 0x7a, 0xec, 0x3d, 0xae,
		0x07, 0xbd, 0xc1, 0xc6, 0x7b, 0xf3, 0x36, 0x09
	];
	let kc: [u8; 56] = [
		0x07, 0xff, 0xf4, 0x18, 0x1a, 0xc6, 0xcc, 0x95, 0xec, 0x1c, 0x16, 0xa9, 0x4a, 0x0f, 0x74, 0xd1,
		0x2d, 0xa2, 0x32, 0xce, 0x40, 0xa7, 0x75, 0x52, 0x28, 0x1d, 0x28, 0x2b, 0xb6, 0x0c, 0x0b, 0x56,
		0xfd, 0x24, 0x64, 0xc3, 0x35, 0x54, 0x39, 0x36, 0x52, 0x1c, 0x24, 0x40, 0x30, 0x85, 0xd5, 0x9a,
		0x44, 0x9a, 0x50, 0x37, 0x51, 0x4a, 0x87, 0x9d
	];
	let mut pa = [0u8; 56];
	let mut pb = [0u8; 56];
	let mut ka = [0u8; 56];
	let mut kb = [0u8; 56];
	x448.xcurve_base(&mut pa[..], &sa[..]).unwrap();
	x448.xcurve_base(&mut pb[..], &sb[..]).unwrap();
	x448.xcurve(&mut ka[..], &sa[..], &pb[..]).unwrap();
	x448.xcurve(&mut kb[..], &sb[..], &pa[..]).unwrap();
	assert_eq!(&pa[..], &pac[..]);
	assert_eq!(&pb[..], &pbc[..]);
	assert_eq!(&ka[..], &kc[..]);
	assert_eq!(&kb[..], &kc[..]);
}

#[test]
fn xcurve_reject_low()
{
	let x25519 = Curve::lookup("curve25519").unwrap();
	let sa: [u8; 32] = [
		0x77, 0x07, 0x6d, 0x0a, 0x73, 0x18, 0xa5, 0x7d, 0x3c, 0x16, 0xc1, 0x72, 0x51, 0xb2, 0x66, 0x45,
		0xdf, 0x4c, 0x2f, 0x87, 0xeb, 0xc0, 0x99, 0x2a, 0xb1, 0x77, 0xfb, 0xa5, 0x1d, 0xb9, 0x2c, 0x2a
	];
	let pm: [u8; 32] = [
		0xE0, 0xEB, 0x7A, 0x7C, 0x3B, 0x41, 0xB8, 0xAE, 0x16, 0x56, 0xE3, 0xFA, 0xF1, 0x9F, 0xC4, 0x6A,
		0xDA, 0x09, 0x8D, 0xEB, 0x9C, 0x32, 0xB1, 0xFD, 0x86, 0x62, 0x05, 0x16, 0x5F, 0x49, 0xB8, 0x00,
		
	];
	let mut res = [0u8; 32];
	x25519.xcurve(&mut res[..], &sa[..], &pm[..]).unwrap_err();
}

/*
	fn ecclib_bigint_load(out: *mut u32, outwords: size_t, in1: *const u8, inoctets: size_t);
	fn ecclib_bigint_store(out: *mut u8, outoctets: size_t, in1: *const u32);
	fn ecclib_field_zero(field: *const Field, out: *mut c_void);
	fn ecclib_field_set_int(field: *const Field, out: *mut c_void, val: u32);
	fn ecclib_field_set(field: *const Field, out: *mut c_void, val: *const u8);
	fn ecclib_field_get(field: *const Field, in1: *const c_void, val: *mut u8);
	fn ecclib_field_check(field: *const Field, val: *const u8) -> c_int;
	fn ecclib_field_mul(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_field_smul(field: *const Field, out: *mut c_void, in1: *const c_void, in2: u32);
	fn ecclib_field_sqr(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_inv(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_sqrt(field: *const Field, out: *mut c_void, in1: *const c_void) -> c_int;
	fn ecclib_field_copy(field: *const Field, out: *mut c_void, in1: *const c_void);
	fn ecclib_field_load_cond(field: *const Field, out: *mut c_void, in1: *const c_void, flag: u32);
	fn ecclib_field_select(field: *const Field, out: *mut c_void, in1: *const c_void, in2: *const c_void,
		flag: u32);
	fn ecclib_field_cswap(field: *const Field, x1: *mut c_void, x2: *mut c_void, flag: u32);

	fn ecclib_curve_order(curve: *const Curve) -> *const u8;
	fn ecclib_curve_montgomery_base(curve: *const Curve) -> *const u8;
	fn ecclib_curve_reduce_partial(curve: *const Curve, output: *mut u32, input: *const u32);
	fn ecclib_curve_reduce_full(curve: *const Curve, output: *mut u8, input: *const u32);
	fn ecclib_curve_point_zero(curve: *const Curve, point: *mut c_void);
	fn ecclib_curve_point_base(curve: *const Curve, point: *mut c_void);
	fn ecclib_curve_point_set(curve: *const Curve, point: *mut c_void, data: *const u8) -> c_int;
	fn ecclib_curve_point_get(curve: *const Curve, point: *const c_void, output: *mut u8);
	fn ecclib_curve_point_add(curve: *const Curve, out: *mut c_void, in1: *const c_void, in2: *const c_void);
	fn ecclib_curve_point_double(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_mul_base(curve: *const Curve, out: *mut c_void, mul: *const u32);
	fn ecclib_curve_point_mul(curve: *const Curve, out: *mut c_void, mul: *const u32, in1: *const c_void);
	fn ecclib_curve_point_neg(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_copy(curve: *const Curve, out: *mut c_void, in1: *const c_void);
	fn ecclib_curve_point_get_coords(curve: *const Curve, point: *const c_void, x: *mut c_void, y: *mut c_void);
	fn ecclib_curve_point_set_coords(curve: *const Curve, point: *mut c_void, x: *const c_void, y: *const c_void)
		-> c_int;
	fn ecclib_curve_point_montgomery(curve: *const Curve, output: *mut c_void, point: *const c_void) -> c_int;
	fn ecclib_curve_montgomery_mul(curve: *const Curve, output: *mut c_void, mul: *const u32,
		base: *const c_void) -> c_int;
	fn ecclib_set_thread_fns(lock: extern fn(ctx: *mut c_void), unlock: extern fn(ctx: *mut c_void),
		ctx: *mut c_void);
	fn ecclib_zeroize(buf: *mut u8, buflen: size_t);
*/

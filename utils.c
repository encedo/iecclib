#include "utils.h"

int streq(const char* left, const char* right)
{
	while(*left && *right) {
		if(*left != *right) return 0;
		left++;
		right++;
	}
	return (*left == *right);
}

void ecclib_zeroize(uint8_t* buf, size_t buflen)
{
	//The "extra" parens turn a variable into reference lvalue, which is volatile. Without those parens,
	//the meaning is subtly different and wrong.
	for(size_t i = 0; i < buflen; i++)
		(((volatile uint8_t*)buf)[i]) = 0;
}
